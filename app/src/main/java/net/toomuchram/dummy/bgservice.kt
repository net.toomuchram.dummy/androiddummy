package net.toomuchram.dummy

import android.content.Intent
import android.os.IBinder
import org.jetbrains.annotations.Nullable
import android.app.*
import android.os.Build
import android.widget.Toast
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import okhttp3.Dispatcher
import org.greenrobot.eventbus.EventBus

import java.net.URI

import org.java_websocket.drafts.Draft_6455

class RomereisBGService : Service() {

    /*------------------*/
    /* Global Variables */
    /*------------------*/

    private lateinit var mWs: RomereisWebsocket

    private lateinit var notifUtils: NotificationUtilities
    private lateinit var accountUtils: AccountUtilities

    /*-----------*/
    /* Functions */
    /*-----------*/

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        if(intent != null){
            val sessionId = accountUtils.sessionId
            //we don't have to check the sessionId, because that's done by the WebSocket itself
            if(intent.getStringExtra("action") == "getCachedLocations"){
                if(sessionId != null) {
                    requestCachedLocations(sessionId)
                }
                else {
                    val message = "{\"channel\":\"errors\", \"error\":\"Niet ingelogd\"}"
                    EventBus.getDefault().post(MessageEvent(message))
                }
            }
        }
        //Start location updates to be sure
        if(mWs.connected){
            mWs.startLocationUpdates()
            mWs.oneTimeLocation()
        }

        return START_STICKY

    }

    //Because of the sleep, it has to be
    //run in a coroutine
    private fun requestCachedLocations(sessionId: String) = CoroutineScope(Dispatchers.IO).launch {
        var i = 0
        val attempts = 10
        while((!mWs.connected || !mWs.loggedIn) && i < attempts) {
            delay(1000)
            i++
        }
        if(i < attempts) {
            mWs.send("{\"channel\":\"getCachedLocations\", \"sessionId\": \"$sessionId\"}")
        }
        else {
            val message = "{\"channel\":\"errors\", \"error\":\"Geen verbinding met de server\"}"
            EventBus.getDefault().post(MessageEvent(message))
        }
    }

    @Nullable
    override fun onBind(intent: Intent?): IBinder? {
        return null
    }

    override fun onCreate(){
        notifUtils = NotificationUtilities(applicationContext)
        accountUtils = AccountUtilities(applicationContext)

        val sessionId = accountUtils.sessionId
        if(sessionId == null){
            //notify the user
            notifUtils.notifyNotLoggedIn()
            //stop the service
            stopService(Intent(applicationContext, RomereisBGService::class.java))
        }
        else{
            //open websocket and connect
            mWs = RomereisWebsocket(
                URI(applicationContext.getString(R.string.JS_server_IP)),
                Draft_6455(),
                sessionId,
                applicationContext,
                this
            )
            mWs.connect()
        }

        println("started service")
    }


    override fun onDestroy() {
        super.onDestroy()
        println("stopped service")
    }

}


