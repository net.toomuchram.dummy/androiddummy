package net.toomuchram.dummy

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.jean.jcplayer.JcPlayerManagerListener
import com.example.jean.jcplayer.general.JcStatus
import com.example.jean.jcplayer.model.JcAudio
import com.example.jean.jcplayer.view.JcPlayerView
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import org.sufficientlysecure.htmltextview.HtmlTextView


class AudioPlayerActivity : AppCompatActivity() {

    private lateinit var jcplayerView: JcPlayerView
    private lateinit var playlist: ArrayList<JcAudio>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_audio_player)


        jcplayerView = findViewById(R.id.jcplayer)

        playlist = try {
            initialisePlaylistWithIntent()
        } catch (e: NoAudioException) {
            initialiseDefaultPlaylist()
        }

        var playlistDescription = "Afspeellijst:<br>"
        for (i in playlist.indices) {
            playlistDescription += "${i + 1}. ${playlist[i].title}<br>"
        }

        findViewById<HtmlTextView>(R.id.playlist_description).setHtml(playlistDescription)
        jcplayerView.initPlaylist(
            playlist,
            PlayerManagerListener(jcplayerView, playlist)
        )
        jcplayerView.createNotification()
    }

    fun initialisePlaylistWithIntent(): ArrayList<JcAudio> {
        val audiofile = intent.getStringExtra("audiofile")
        val audioname = intent.getStringExtra("audioname")

        if (audiofile == null || audioname == null) {
            throw NoAudioException("Missing audio filename and title")
        }

        val playlist: ArrayList<JcAudio> = ArrayList()
        playlist.add(JcAudio.createFromAssets(audioname, audiofile))
        return playlist
    }

    fun initialiseDefaultPlaylist(): ArrayList<JcAudio> {
        val playlist: ArrayList<JcAudio> = ArrayList()
        val audioFilesArray = resources.getStringArray(R.array.audiofiles)
        val audioNamesArray = resources.getStringArray(R.array.audionames)

        for (i in audioFilesArray.indices) {
            val audiofile = audioFilesArray[i]
            val audioname = audioNamesArray[i]
            playlist.add(JcAudio.createFromAssets(audioname, audiofile))
        }

        return playlist
    }

    class NoAudioException(message: String): Exception(message)

    class PlayerManagerListener(
        private val jcPlayerView: JcPlayerView,
        private val playlist: ArrayList<JcAudio>
    ) : JcPlayerManagerListener {
        override fun onCompletedAudio() {}
        override fun onContinueAudio(status: JcStatus) {}
        override fun onJcpError(throwable: Throwable) {}
        override fun onPaused(status: JcStatus) {}
        override fun onPlaying(status: JcStatus) {}
        override fun onPreparedAudio(status: JcStatus) {}

        override fun onStopped(status: JcStatus) {
            CoroutineScope(Dispatchers.Main).launch {
                // wait 500 millis so the new audio is loaded
                delay(500)
                if (
                    status.playState == JcStatus.PlayState.STOP &&
                    status.jcAudio == null
                ) {
                    jcPlayerView.playAudio(playlist[0])
                    delay(100)
                    jcPlayerView.pause()
                }
            }
        }

        override fun onTimeChanged(status: JcStatus) {}
    }

    override fun onDestroy() {
        super.onDestroy()
        jcplayerView.pause()
        jcplayerView.kill()
    }
}
