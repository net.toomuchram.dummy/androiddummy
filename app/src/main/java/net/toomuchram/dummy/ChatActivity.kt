package net.toomuchram.dummy

import android.annotation.SuppressLint
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import org.json.JSONObject
import android.content.Intent
import android.graphics.Bitmap
import java.io.ByteArrayOutputStream
import android.provider.MediaStore
import android.app.Activity
import android.graphics.BitmapFactory
import android.graphics.Color
import android.graphics.Point
import android.os.Environment
import android.view.Gravity
import android.widget.*
import androidx.annotation.Dimension
import androidx.core.content.FileProvider
import com.google.gson.Gson
import org.sufficientlysecure.htmltextview.HtmlTextView
import java.io.File
import java.io.IOException
import java.sql.Timestamp
import kotlin.math.roundToInt
import kotlin.properties.Delegates

class ChatActivity : AppCompatActivity() {

    data class ChatMessage (
        val id: Int,
        val imgpath: String,
        val timestamp: String,
        val user: String,
        val message: String
    )

    lateinit var utils: ChatActivityUtilities

    private lateinit var accountUtils: AccountUtilities
    private lateinit var appUtils: AppUtilities
    private lateinit var photoBoardUtils: PhotoBoardUtilities
    private var adminchat = false

    var loadedDays = 0
    var photoBoardMode = false

    private val activityScope = CoroutineScope(Dispatchers.Main)

    @SuppressLint("ClickableViewAccessibility")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_chat)

        adminchat = intent.getBooleanExtra("adminchat", false)
        utils = ChatActivityUtilities(this, adminchat)
        accountUtils = AccountUtilities(applicationContext)
        appUtils = AppUtilities(applicationContext)

        if(intent != null && intent.getBooleanExtra("photoboard", false)) {
            //Photoboard should be initialised instead of the chat
            photoBoardMode = true
            photoBoardUtils = PhotoBoardUtilities(this)
            photoBoardUtils.initPhotoBoard()
        }
        else {
            photoBoardMode = false
            utils.initChat()
        }
    }

    /* Onclick function can't be moved to the utilities */

    fun onClickSend(view: View) {

        //Temporarily disable the button so the user doesn't spam click it
        //and crash the app
        view.isEnabled = false

        Toast.makeText(
            applicationContext, getString(R.string.message_being_sent),
            Toast.LENGTH_SHORT
        ).show()

        val v = findViewById<EditText>(R.id.chatbox)
        val sessionId = accountUtils.sessionId
        if (sessionId != null) {
            activityScope.launch {
                utils.sendMessage(v.text.toString(), sessionId)
                v.text.clear()
                view.isEnabled = true
            }
        }
    }

    private var imageFilePath: String? = null

    @Throws(IOException::class)
    private fun createImageFile(): File? {
        val imageFileName = "temp"
        val storageDir =
            getExternalFilesDir(Environment.DIRECTORY_PICTURES)
        val image = File.createTempFile(
            imageFileName,  /* prefix */
            ".jpg",         /* suffix */
            storageDir      /* directory */
        )

        imageFilePath = image.absolutePath
        return image
    }

    fun takePhoto() {
        val pictureIntent = Intent(
            MediaStore.ACTION_IMAGE_CAPTURE
        )
        if (pictureIntent.resolveActivity(packageManager) != null) {
            // hacky code that basically tells the camera to save the image
            // somewhere temporarily, then in the onActivityResult that
            // file is retrieved.
            try {
                val photoFile = createImageFile()
                if (photoFile != null) {
                    val photoURI = FileProvider.getUriForFile(
                        this,
                        "net.toomuchram.dummy.provider",
                        photoFile
                    )
                    pictureIntent.putExtra(
                        MediaStore.EXTRA_OUTPUT,
                        photoURI
                    )
                    startActivityForResult(
                        pictureIntent,
                        1
                    )
                }
            } catch (ex: IOException) {
                ex.printStackTrace()
            }

        }
    }

    enum class MessagePosition {
        Bottom,
        Top
    }

    fun addMessage(message: ChatMessage, withHeader: Boolean = true, position: MessagePosition = MessagePosition.Bottom) {
        val chatContainer = findViewById<LinearLayout>(R.id.chat_content)
        val scrollView = findViewById<ScrollView>(R.id.scrollView3)

        val messageBuilder = RomereisChatMessageBuilder(this, message, photoBoardMode)
        messageBuilder.withHeader = withHeader
        val messageContainer = messageBuilder.build()

        activityScope.launch {
            if (position == MessagePosition.Top) {
                chatContainer.addView(messageContainer, 0)
                // don't scroll to bottom if the container is added at the top
            } else {
                chatContainer.addView(messageContainer)
                scrollView.post {
                    scrollView.fullScroll(ScrollView.FOCUS_DOWN)
                }
            }
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onMessageEvent(event: MessageEvent) {
        val gson = Gson()

        val wsMessage = JSONObject(event.message)
        val channel = wsMessage.getString("channel")
        if (channel != "msg" && channel != "adminmsg") {
            return
        }
        if (channel == "adminmsg" && !adminchat) {
            return
        }

        val chatMessage = gson.fromJson(wsMessage.toString(), ChatMessage::class.java)

        val loadedMessages = utils.loadedMessages

        // Check if the message is already loaded to avoid duplicates
        if (loadedMessages[chatMessage.id] != null) {
            return
        }

        val previousMessage = loadedMessages[chatMessage.id - 1]
        if (
            previousMessage != null &&
            previousMessage.user == chatMessage.user
        ) {

            addMessage(message = chatMessage, withHeader = false)
        } else {
            addMessage(chatMessage)
        }
        utils.loadedMessages[chatMessage.id] = chatMessage
    }


    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        if (utils.verifyCameraPerms()) {
            takePhoto()
        } else {
            Toast.makeText(
                applicationContext, getString(R.string.photo_perms_explanation),
                Toast.LENGTH_SHORT
            ).show()
        }
    }

    public override fun onActivityResult(
        requestCode: Int,
        resultCode: Int,
        data: Intent?
    ) {
        super.onActivityResult(requestCode, resultCode, data)

        /* Camera related code */
        if (resultCode == Activity.RESULT_OK && imageFilePath != null) {

            val bitmap = BitmapFactory.decodeFile(imageFilePath)

            //Calculate the new image size, so it's not too big
            //There is no logic.
            var width = bitmap.width.toFloat()
            var height = bitmap.height
            var divisionfactor = 1.5F
            while (width > 700) {
                width = width.div(divisionfactor)
            }
            divisionfactor = width.div(bitmap.width)
            height = if (divisionfactor > 1) {
                height.div(divisionfactor).roundToInt()
            } else {
                (height * divisionfactor).roundToInt()
            }

            val scaledbitmap = Bitmap.createScaledBitmap(
                bitmap,
                width.roundToInt(), height, true
            )
            //end there is no logic

            val v = findViewById<EditText>(R.id.chatbox)
            val sessionId = accountUtils.sessionId
            if (sessionId != null) {
                //convert the photo to a byte array
                val stream = ByteArrayOutputStream()
                scaledbitmap.compress(Bitmap.CompressFormat.PNG, 100, stream)
                val byteArray = stream.toByteArray()

                //create a new intent
                val intent =
                    Intent(this@ChatActivity, PhotoEditActivity::class.java)
                //...and attach the bytearray to the newly created intent
                intent.putExtra("bitmap", byteArray)
                intent.putExtra("adminchat", adminchat)

                intent.putExtra("caption", v.text.toString())
                v.text.clear()

                startActivity(intent)
            }
        }
    }
}