package net.toomuchram.dummy

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import net.toomuchram.dummy.ui.login.UsernameActivity
import android.widget.Toast
import android.widget.EditText
import android.widget.ProgressBar
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import kotlinx.coroutines.launch


class SendNotifActivity : AppCompatActivity() {

    private val activityScope = CoroutineScope(Dispatchers.Main)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_send_notif)
    }

    //DO NOT REMOVE THE VIEW OPTION
    fun sendNotif(v: View) {
        val message = (findViewById<EditText>(R.id.notifInput)).text.toString()
        val sessionId = AccountUtilities(applicationContext).sessionId
        if (sessionId == null || sessionId == "") {
            val intent = Intent(this, UsernameActivity::class.java)
            startActivity(intent)
        } else {
            if (message == "") {
                Toast.makeText(
                    applicationContext, "Voer een bericht in",
                    Toast.LENGTH_SHORT
                ).show()
            } else {
                val spinner = findViewById<ProgressBar>(R.id.progressBar2)
                spinner.visibility = View.VISIBLE

                activityScope.launch {
                    try {
                        val resp2 = CoroutineScope(Dispatchers.IO).async {
                             khttp.post(
                                url = getString(R.string.PHP_server_IP) +
                                        "/sendNotification",
                                data = mapOf(
                                    "sessionId" to sessionId,
                                    "message" to message
                                )
                            ).text
                        }
                        val resp = resp2.await()
                        if (
                            resp == "ERR_NO_SUCH_SESSION"
                            || resp == "ERR_NO_SUCH_USER"
                            || resp == "ERR_MISSING_SESSIONID"
                        ) {
                            val intent = Intent(
                                applicationContext,
                                UsernameActivity::class.java
                            )
                            startActivity(intent)
                        } else if (
                            resp == "ERR_UNAUTHORISED"
                        ) {
                            Toast.makeText(
                                applicationContext, "Niet toegestaan",
                                Toast.LENGTH_LONG
                            ).show()
                        } else {
                            Toast.makeText(
                                applicationContext, "Verzonden",
                                Toast.LENGTH_SHORT
                            ).show()
                            finish()

                        }
                        spinner.visibility = View.GONE
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
            }
        }

    }
}
