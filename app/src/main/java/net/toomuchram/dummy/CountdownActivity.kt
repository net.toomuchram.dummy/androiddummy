package net.toomuchram.dummy

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import cn.iwgang.countdownview.CountdownView
import android.os.CountDownTimer
import android.widget.Toast
import com.app.progresviews.ProgressWheel
import java.util.concurrent.TimeUnit
import kotlin.math.roundToInt


class CountdownActivity : AppCompatActivity() {

    private lateinit var appUtils: AppUtilities

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_countdown)

        appUtils = AppUtilities(applicationContext)

        val millisRemaining = appUtils.getRemainingMillis()
        if(millisRemaining < 0) {
            Toast.makeText(
                this, getString(R.string.romereis_already_begun),
                Toast.LENGTH_LONG
            ).show()
            finish()
        }

        val countdownView = findViewById<CountdownView>(R.id.countdownview)
        countdownView.updateShow(millisRemaining)

        val progressbar = findViewById<ProgressWheel>(R.id.progress_wheel)

        object : CountDownTimer(millisRemaining, 1000) {

            override fun onTick(millisUntilFinished: Long) {
                countdownView.updateShow(millisUntilFinished)
                // As it turns out, 360=100%
                progressbar.setPercentage(((TimeUnit.MILLISECONDS.toDays(millisUntilFinished).div(365f))*360).roundToInt())
            }

            override fun onFinish() {
                finish()
            }
        }.start()

    }
}
