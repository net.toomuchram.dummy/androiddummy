package net.toomuchram.dummy

import android.Manifest
import android.accounts.NetworkErrorException
import android.annotation.SuppressLint
import android.app.*
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.BitmapFactory
import android.location.Location
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.google.android.gms.location.LocationServices
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.iid.FirebaseInstanceId
import khttp.post
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import java.net.HttpURLConnection
import java.net.URL
import java.io.*
import com.snatik.storage.Storage
import android.graphics.Bitmap
import android.util.Log
import android.util.TypedValue
import android.widget.Toast
import androidx.work.*
import com.mapbox.mapboxsdk.Mapbox
import com.mapbox.mapboxsdk.geometry.LatLng
import com.mapbox.mapboxsdk.geometry.LatLngBounds
import com.mapbox.mapboxsdk.maps.Style
import com.mapbox.mapboxsdk.offline.OfflineManager
import com.mapbox.mapboxsdk.offline.OfflineRegion
import com.mapbox.mapboxsdk.offline.OfflineTilePyramidRegionDefinition
import com.mapbox.mapboxsdk.plugins.offline.model.NotificationOptions
import com.mapbox.mapboxsdk.plugins.offline.model.OfflineDownloadOptions
import com.mapbox.mapboxsdk.plugins.offline.offline.OfflinePlugin
import com.mapbox.mapboxsdk.plugins.offline.utils.OfflineUtils
import kotlinx.coroutines.withContext
import net.toomuchram.dummy.data.LoginDataSource
import net.toomuchram.dummy.data.LoginRepository
import java.util.*
import java.util.concurrent.TimeUnit
import javax.net.ssl.SSLHandshakeException


/*

Code that is not in an `override` function should be put in a Utility class.
If it's useful for the entire app, it should be put in here.

    Useful to           Should be put in
    ==========================================
    Entire app          AppUtilities
    Notifications       NotificationsUtilities
    Account management  AccountUtilities
    MainActivity        MainActivityUtilities

Pretty simple, right?

 */


class AppUtilities(private val c: Context) {

    private val networkScope = CoroutineScope(Dispatchers.IO)

    fun requestLocationPermissions(activity: Activity) {
        ActivityCompat.requestPermissions(
            activity,
            arrayOf(Manifest.permission.ACCESS_FINE_LOCATION), 101
        )
    }

    fun verifyLocationPermissions(): Boolean {
        return ContextCompat.checkSelfPermission(
            c,
            Manifest.permission.ACCESS_FINE_LOCATION
        ) == PackageManager.PERMISSION_GRANTED
    }

    //the return value represents the session validity
    fun sendLocationToServerAsync(location: Location, sessionId: String) =
        networkScope.async {
            try {
                val resp = post(
                    url = c.getString(R.string.PHP_server_IP) + "/postLocation",
                    data = mapOf(
                        "sessionId" to sessionId,
                        "lat" to location.latitude.toString(),
                        "lon" to location.longitude.toString()
                    )
                ).text
                if (resp == "ERR_NO_SUCH_SESSION" || resp == "ERR_NO_SUCH_USER") {
                    return@async false
                }
                return@async true
            } catch (e: Exception) {
                e.printStackTrace()
                return@async true
            }
        }

    @SuppressLint("MissingPermission")
    fun getOneTimeLocation(callback: (location: Location?) -> Unit) {
        if (verifyLocationPermissions()) {
            val fusedLocationClient =
                LocationServices.getFusedLocationProviderClient(c)
            fusedLocationClient.lastLocation
                .addOnSuccessListener { location: Location? ->
                    callback.invoke(location)
                }
        } else {
            //invoke null if location permissions aren't granted
            callback.invoke(null)
        }
    }

    //function to send the firebase token to server
    //the return value represents the session validity
    fun sendFirebaseTokenToServerAsync(token: String, sessionId: String) =
        networkScope.async {
            try {
                val resp = post(
                    url = c.getString(R.string.PHP_server_IP) + "/postDeviceToken/Android",
                    data = mapOf(
                        "token" to token,
                        "sessionId" to sessionId
                    )
                ).text
                if (resp == "ERR_NO_SUCH_SESSION" || resp == "ERR_NO_SUCH_USER") {
                    return@async false
                }
                return@async true
            } catch (e: Exception) {
                e.printStackTrace()
                return@async true
            }
        }

    fun getFirebaseToken(callback: (String?) -> Unit) {
        FirebaseInstanceId.getInstance().instanceId
            .addOnCompleteListener(OnCompleteListener { task ->
                if (!task.isSuccessful) {
                    println("getInstanceId failed: " + task.exception)
                    callback.invoke(null)
                    return@OnCompleteListener
                }


                // Return new Instance ID token
                val token = task.result?.token
                callback.invoke(token)
            })
    }

    //This function should not be in use, since it didn't work well.
    //Do not remove it from the code though; I may come back to it once
    fun setAlarm() {
        val constraints = Constraints.Builder()
            .setRequiredNetworkType(NetworkType.CONNECTED)
            .build()
        val locationWork = PeriodicWorkRequestBuilder<RomereisLocationWorker>(
            15,
            TimeUnit.MINUTES
        )
            .setConstraints(constraints)
            .build()
        WorkManager.getInstance().enqueue(locationWork)


    }

    //name says it all
    fun checkInternetConnectionAsync() = networkScope.async {
        try {
            val resp =
                khttp.get(url = "https://toomuchram.net/captiveportal.txt").text
            if (resp == "success") {
                return@async true
            }
            return@async false
        } catch (e: NetworkErrorException) {
            e.printStackTrace()
            return@async false
        } catch (e: SSLHandshakeException) {
            println("Internet connection check is offline")
            e.printStackTrace()
            return@async true
        }
    }


    //https://stackoverflow.com/questions/18953632/how-to-set-image-from-url-for-imageview

    fun getBitmapFromURLAsync(src: String) = networkScope.async {
        try {
            return@async withContext(Dispatchers.IO) {
                val url = URL(src)
                val connection = url.openConnection() as HttpURLConnection
                connection.doInput = true
                connection.connect()
                val input = connection.inputStream
                return@withContext BitmapFactory.decodeStream(input)
            }

        } catch (e: IOException) {
            e.printStackTrace()
            return@async null
        }
    }

    suspend fun getAndSaveHtmlContent(name: String, sessionId: String) {
        val content = getPageAsync(name, sessionId)

        if (content != null) {
            writeToFile(
                content, "$name.html"
            )
        } else {
            val existingContent = readFromFile("$name.html")
            if (existingContent == "" || existingContent == null) {
                writeToFile(
                    "$name.html is nog niet gedownload.", "$name.html"
                )
            }
        }
    }

    fun writeToFile(data: String, filename: String) {
        val storage = Storage(c)
        val storagepath = storage.internalFilesDirectory
        val filepath = "$storagepath/$filename"
        if (!storage.isFileExist(filepath)) {
            storage.createFile(filepath, data)
        } else {
            //recreate the file
            storage.deleteFile(filepath)
            storage.createFile(filepath, data)
        }
    }

    fun readFromFile(filename: String): String? {
        val storage = Storage(c)
        val storagepath = storage.internalFilesDirectory
        val filepath = "$storagepath/$filename"
        return storage.readTextFile(filepath)
    }

    suspend fun getPageAsync(page: String, sessionId: String): String?  {
        return withContext(Dispatchers.IO) {
            try {
                val resp =
                    khttp.get(url = c.getString(R.string.PHP_server_IP) + "/dynamicContent?sessionId=" + sessionId + "&name=" + page)
                if (resp.text == "ERR_NO_SUCH_SESSION" || resp.text == "ERR_NO_SUCH_USER" || resp.statusCode != 200) {
                    return@withContext null
                }
                return@withContext resp.text
            } catch (e: Exception) {
                e.printStackTrace()
                return@withContext null
            }
        }
    }

    fun enlargeImage(bitmap: Bitmap, activity: Activity) {

        val intent = Intent(activity, ImageActivity::class.java)
        //convert the photo to a byte array and attach it to the newly created intent
        val stream = ByteArrayOutputStream()
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream)
        val byteArray = stream.toByteArray()
        intent.putExtra("bitmap", byteArray)
        activity.startActivity(intent)
    }

    // https://stackoverflow.com/questions/4605527/converting-pixels-to-dp
    fun convertDpToPixel(dp: Float): Float {
        return TypedValue.applyDimension(
            TypedValue.COMPLEX_UNIT_DIP,
            dp, c.resources.displayMetrics
        )
    }

    fun logOut(activity: Activity) {
        val loginDataSource = LoginDataSource(activity)
        val loginRepository = LoginRepository(loginDataSource)
        //stop the service
        //service has to be stopped BEFORE logout
        //so the logged out notification doesn't pop up
        activity.stopService(Intent(c, RomereisBGService::class.java))
        //log out
        loginRepository.logout()
        //close the app
        activity.finishAffinity()
    }

    // Calculate the remaining milliseconds until the Romereis
    fun getRemainingMillis(): Long {
        val date = Date()
        val calendar = Calendar.getInstance()
        calendar.clear() // Sets hours/minutes/seconds/milliseconds to zero
        // 2020-3-7 7:20
        calendar.set(2020, 2, 7, 7, 20)
        val dateToRR = calendar.time
        return dateToRR.time - date.time
    }

    //download map
    fun downloadRomeMap() {
        // Mapbox access token is configured here. This needs to be called either in your application
        // object or in the same activity which contains the mapview.
        Mapbox.getInstance(c, c.getString(R.string.mapbox_access_token))
        Mapbox.getTelemetry()?.setUserTelemetryRequestState(false)

        // Set up the OfflineManager
        val offlineManager = OfflineManager.getInstance(c)
        // Get the region name
        val regionName = c.getString(R.string.offine_map_download_name)
        offlineManager.listOfflineRegions(object :
            OfflineManager.ListOfflineRegionsCallback {
            override fun onList(offlineRegions: Array<OfflineRegion>) {
                var mapDownloaded = false
                for (region in offlineRegions) {
                    if (OfflineUtils.convertRegionName(region.metadata) == regionName) {
                        mapDownloaded = true
                    }
                }
                if(!mapDownloaded) {
                    downloadRomeMap2()
                }
            }

            override fun onError(error: String?) {
                println("Error during deletion of Rome map: $error")
            }
        })
    }
    //Second part of the function
    fun downloadRomeMap2() {

        val regionName = c.getString(R.string.offine_map_download_name)
        // Define region of map tiles
        val definition = OfflineTilePyramidRegionDefinition(
            Style.MAPBOX_STREETS, LatLngBounds.Builder()
                .include(LatLng(41.8772, 12.4441))
                .include(LatLng(41.9115, 12.5252))
                .build(),
            12.0,
            16.0,
            c.resources.displayMetrics.density
        )
        // Customize the download notification's appearance
        val notificationOptions = NotificationOptions.builder(c)
            .returnActivity(MainActivity::class.java.name)
            .build()
        // Start downloading the map tiles for offline use
        OfflinePlugin.getInstance(c).startDownload(
            OfflineDownloadOptions.builder()
                .definition(definition)
                .metadata(OfflineUtils.convertRegionName(regionName))
                .notificationOptions(notificationOptions)
                .build()
        )
    }

    fun deleteRomeMap() {

        Mapbox.getInstance(c, c.getString(R.string.mapbox_access_token))
        Mapbox.getTelemetry()?.setUserTelemetryRequestState(false)

        // Set up the OfflineManager
        val offlineManager = OfflineManager.getInstance(c)

        offlineManager.listOfflineRegions(RRDeleteRegionCallback(c))
    }

    class RRDeleteRegionCallback(private val c: Context): OfflineManager.ListOfflineRegionsCallback {
        override fun onList(offlineRegions: Array<OfflineRegion>) {
            // Get the region name
            val regionName = c.getString(R.string.offine_map_download_name)

            for (region in offlineRegions) {
                if (OfflineUtils.convertRegionName(region.metadata) == regionName) {
                    region.delete(object :
                        OfflineRegion.OfflineRegionDeleteCallback {
                        override fun onDelete() {
                            Toast.makeText(
                                c,
                                c.getString(R.string.map_deleted),
                                Toast.LENGTH_LONG
                            ).show()
                        }

                        override fun onError(error: String) {
                            println("Error during deletion of Rome map: $error")
                        }
                    })
                }
            }
        }

        override fun onError(error: String?) {
            println("Error during deletion of Rome map: $error")
        }
    }
}