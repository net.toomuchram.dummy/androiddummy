package net.toomuchram.dummy

import android.app.Activity
import android.content.Intent
import android.graphics.*
import android.graphics.drawable.Drawable
import android.util.Log
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import androidx.cardview.widget.CardView
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat
import com.mapbox.mapboxsdk.geometry.LatLng
import com.mapbox.mapboxsdk.maps.Style
import com.google.gson.Gson
import com.google.gson.JsonParser
import com.mapbox.mapboxsdk.plugins.annotation.SymbolOptions
import org.sufficientlysecure.htmltextview.HtmlTextView
import com.mapbox.mapboxsdk.plugins.annotation.Symbol
import com.mapbox.mapboxsdk.style.layers.Property
import com.sothree.slidinguppanel.SlidingUpPanelLayout
import kotlinx.android.synthetic.main.activity_maps.view.*


class MapsActivityUtilities(private val activity: Activity) {
    fun createMarkers(): List<SymbolOptions> {

        val mARKER_SIZE = 0.13f
        val gEOTRACKER_ASSETS_DIR = "geotracker/gebouwen/"

        val forumromanumPos = LatLng(41.892470, 12.485329)
        val forumromanum = MarkerBuilder()
            .position(forumromanumPos)
            .setTitle("Forum Romanum")
            .setSubtitle("Centrum van het oude Rome")
            .setMarkerType(activity.getString(R.string.geotracker_marker_icon_name))
            .setIconSize(mARKER_SIZE)
            .setImages(
                listOf(
                    gEOTRACKER_ASSETS_DIR + "forumromanum0.jpg",
                    gEOTRACKER_ASSETS_DIR + "forumromanum1.jpg"
                )
            )
            .setContentFile(gEOTRACKER_ASSETS_DIR + "forumromanum.html")
            .setIconAnchor(Property.ICON_ANCHOR_BOTTOM)
            .build()

        val cloacamaximaPos = LatLng(41.888902, 12.480262)
        val cloacamaxima = MarkerBuilder()
            .position(cloacamaximaPos)
            .setTitle("Cloaca Maxima")
            .setSubtitle("Rioleringssysteem")
            .setMarkerType(activity.getString(R.string.geotracker_marker_icon_name))
            .setIconSize(mARKER_SIZE)
            .setImages(
                listOf(
                    gEOTRACKER_ASSETS_DIR + "cloacamaxima.jpg",
                    gEOTRACKER_ASSETS_DIR + "cloaca-maxima-binnenkant.jpg"
                )
            )
            .setContentFile(gEOTRACKER_ASSETS_DIR + "cloacamaxima.html")
            .setIconAnchor(Property.ICON_ANCHOR_BOTTOM)
            .build()

        val servianWallPos = LatLng(41.90176, 12.50175)
        val servianWall = MarkerBuilder()
            .position(servianWallPos)
            .setTitle("Servische Muur")
            .setSubtitle("Restanten van de Servische Muur")
            .setMarkerType(activity.getString(R.string.geotracker_marker_icon_name))
            .setIconSize(mARKER_SIZE)
            .setIconAnchor(Property.ICON_ANCHOR_BOTTOM)
            .setImages(
                listOf(
                    gEOTRACKER_ASSETS_DIR + "servianwall.jpg"
                )
            )
            .setContentFile(gEOTRACKER_ASSETS_DIR + "servianwall.html")
            .build()


        val forumboariumPos = LatLng(41.888748, 12.480746)
        val forumboarium = MarkerBuilder()
            .position(forumboariumPos)
            .setTitle("Forum Boarium")
            .setSubtitle("Koeienforum: veemarkt + haven")
            .setMarkerType(activity.getString(R.string.geotracker_marker_icon_name))
            .setIconSize(mARKER_SIZE)
            .setImages(
                listOf(
                    gEOTRACKER_ASSETS_DIR + "forumboarium0.jpg",
                    gEOTRACKER_ASSETS_DIR + "forumboarium1.jpg"
                )
            )
            .setContentFile(gEOTRACKER_ASSETS_DIR + "forumboarium.html")
            .setIconAnchor(Property.ICON_ANCHOR_BOTTOM)
            .build()

        val forumjuliumPos = LatLng(41.893930, 12.485039)
        val forumjulium = MarkerBuilder()
            .position(forumjuliumPos)
            .setTitle("Forum Julium")
            .setSubtitle("Forum van Caesar")
            .setMarkerType(activity.getString(R.string.geotracker_marker_icon_name))
            .setIconSize(mARKER_SIZE)
            .setImages(
                listOf(
                    gEOTRACKER_ASSETS_DIR + "forumjulium.jpg",
                    "bijlagen/reisverslag/forumjulium.webp"
                )
            )
            .setContentFile(gEOTRACKER_ASSETS_DIR + "forumjulium.html")
            .setIconAnchor(Property.ICON_ANCHOR_BOTTOM)
            .build()

        val largoDiTorreArgentinaPos = LatLng(41.895411, 12.476860)
        val largoDiTorreArgentina = MarkerBuilder()
            .position(largoDiTorreArgentinaPos)
            .setTitle("Largo Di Torre Argentina")
            .setSubtitle("Kattenforum")
            .setMarkerType(activity.getString(R.string.geotracker_marker_icon_name))
            .setIconSize(mARKER_SIZE)
            .setImages(
                listOf(
                    gEOTRACKER_ASSETS_DIR + "largoditorreargentina0.jpg",
                    gEOTRACKER_ASSETS_DIR + "largoditorreargentina1.jpg"
                )
            )
            .setContentFile(gEOTRACKER_ASSETS_DIR + "largoditorreargentina.html")
            .setIconAnchor(Property.ICON_ANCHOR_BOTTOM)
            .build()

        val tibereilandPos = LatLng(41.890466, 12.477742)
        val tibereiland = MarkerBuilder()
            .position(tibereilandPos)
            .setTitle("Tibereiland")
            .setSubtitle("Eiland in de Tiber met ziekenhuis")
            .setMarkerType(activity.getString(R.string.geotracker_marker_icon_name))
            .setIconSize(mARKER_SIZE)
            .setImages(
                listOf(
                    gEOTRACKER_ASSETS_DIR + "tibereiland.jpg"
                )
            )
            .setContentFile(gEOTRACKER_ASSETS_DIR + "tibereiland.html")
            .setIconAnchor(Property.ICON_ANCHOR_BOTTOM)
            .build()

        val theatreOfMarcellusPos = LatLng(41.89194, 12.47975)
        val theatreOfMarcellus = MarkerBuilder()
            .position(theatreOfMarcellusPos)
            .setTitle("Theater van Marcellus")
            .setSubtitle("Theater gebouwd door Caesar")
            .setMarkerType(activity.getString(R.string.geotracker_marker_icon_name))
            .setIconSize(mARKER_SIZE)
            .setImages(
                listOf(
                    gEOTRACKER_ASSETS_DIR + "theatervanmarcellus0.jpg",
                    gEOTRACKER_ASSETS_DIR + "theatervanmarcellus1.jpg"
                )
            )
            .setContentFile(gEOTRACKER_ASSETS_DIR + "theatervanmarcellus.html")
            .setIconAnchor(Property.ICON_ANCHOR_BOTTOM)
            .build()

        val forumAugustumPos = LatLng(41.89429, 12.48681)
        val forumAugustum = MarkerBuilder()
            .position(forumAugustumPos)
            .setTitle("Forum Augustum")
            .setSubtitle("Forum van Augustus")
            .setMarkerType(activity.getString(R.string.geotracker_marker_icon_name))
            .setIconSize(mARKER_SIZE)
            .setImages(
                listOf(
                    gEOTRACKER_ASSETS_DIR + "forumaugustum.jpg"
                )
            )
            .setContentFile(gEOTRACKER_ASSETS_DIR + "forumaugustum.html")
            .setIconAnchor(Property.ICON_ANCHOR_BOTTOM)
            .build()

        val araPacisPos = LatLng(41.90613, 12.47546)
        val araPacis = MarkerBuilder()
            .position(araPacisPos)
            .setTitle("Ara Pacis")
            .setSubtitle("Vredesaltaar van Augustus")
            .setMarkerType(activity.getString(R.string.geotracker_marker_icon_name))
            .setIconSize(mARKER_SIZE)
            .setIconAnchor(Property.ICON_ANCHOR_BOTTOM)
            .setImages(
                listOf(
                    gEOTRACKER_ASSETS_DIR + "arapacis-voorkant.jpg",
                    gEOTRACKER_ASSETS_DIR + "arapacis-achterkant.jpg"
                )
            )
            .setContentFile(gEOTRACKER_ASSETS_DIR + "arapacis.html")
            .build()

        val mausoleumOfAugustusPos = LatLng(41.90602, 12.47642)
        val mausoleumOfAugustus = MarkerBuilder()
            .position(mausoleumOfAugustusPos)
            .setTitle("Mausoleum van Augustus")
            .setSubtitle("Begraafplaats van Augustus en andere keizers")
            .setMarkerType(activity.getString(R.string.geotracker_marker_icon_name))
            .setIconSize(mARKER_SIZE)
            .setIconAnchor(Property.ICON_ANCHOR_BOTTOM)
            .setImages(
                listOf(
                    gEOTRACKER_ASSETS_DIR + "mausoleumvanaugustus.jpg"
                )
            )
            .setContentFile(gEOTRACKER_ASSETS_DIR + "mausoleumaugustum.html")
            .build()

        val templeOfAgrippaPos = LatLng(41.8986, 12.47683)
        val templeOfAgrippa = MarkerBuilder()
            .position(templeOfAgrippaPos)
            .setTitle("Tempel van Agrippa (Pantheon)")
            .setSubtitle("Tempel van een vriend van Augustus; later omgebouwd tot het Pantheon")
            .setMarkerType(activity.getString(R.string.geotracker_marker_icon_name))
            .setIconSize(mARKER_SIZE)
            .setIconAnchor(Property.ICON_ANCHOR_BOTTOM)
            .setImages(
                listOf(
                    gEOTRACKER_ASSETS_DIR + "pantheon0.jpg",
                    gEOTRACKER_ASSETS_DIR + "pantheon1.jpg"
                )
            )
            .setContentFile(gEOTRACKER_ASSETS_DIR + "tempelvanagrippa.html")
            .build()

        val domusAureaPos = LatLng(41.890896, 12.495370)
        val domusAurea = MarkerBuilder()
            .position(domusAureaPos)
            .setTitle("Domus Aurea")
            .setSubtitle("Paleis van Nero")
            .setMarkerType(activity.getString(R.string.geotracker_marker_icon_name))
            .setIconSize(mARKER_SIZE)
            .setIconAnchor(Property.ICON_ANCHOR_BOTTOM)
            .setImages(
                listOf(
                    gEOTRACKER_ASSETS_DIR + "domusaurea.jpg",
                    "bijlagen/domusaurea.jpg"
                )
            )
            .setContentFile(gEOTRACKER_ASSETS_DIR + "domusaurea.html")
            .build()

        val hotelPos = LatLng(41.901790, 12.503949)
        val hotel = MarkerBuilder()
            .position(hotelPos)
            .setTitle("Hotel")
            .setSubtitle("Hotel Luciani<br>Via Marsala 96 Roma, Itali&euml;")
            .setContentFile("geotracker/hotel/beschrijving.html")
            .setIconSize(0.6f)
            .setMarkerType(activity.getString(R.string.geotracker_hotel_icon_name))
            .setImages(
                listOf(
                    "geotracker/hotel/buitenkant.jpg",
                    "geotracker/hotel/binnenkant.jpg"
                )
            )
            .build()

        val colosseumPos = LatLng(41.89025, 12.49229)
        val colosseum = MarkerBuilder()
            .position(colosseumPos)
            .setTitle("Colosseum")
            .setSubtitle("Amphitheatrum Flavium")
            .setMarkerType(activity.getString(R.string.geotracker_marker_icon_name))
            .setIconSize(mARKER_SIZE)
            .setIconAnchor(Property.ICON_ANCHOR_BOTTOM)
            .setImages(
                listOf(
                    gEOTRACKER_ASSETS_DIR + "colosseum-binnenkant.webp",
                    gEOTRACKER_ASSETS_DIR + "colosseum-buiten.jpg"
                )
            )
            .setContentFile(gEOTRACKER_ASSETS_DIR + "colosseum.html")
            .build()

        val forumTrajanumPos = LatLng(41.89521, 12.48560)
        val forumTrajanum = MarkerBuilder()
            .position(forumTrajanumPos)
            .setTitle("Forum Trajanum")
            .setSubtitle("Grootste en laatste keizersforum")
            .setMarkerType(activity.getString(R.string.geotracker_marker_icon_name))
            .setIconSize(mARKER_SIZE)
            .setIconAnchor(Property.ICON_ANCHOR_BOTTOM)
            .setImages(
                listOf(
                    gEOTRACKER_ASSETS_DIR + "forumtrajanum0.jpg",
                    gEOTRACKER_ASSETS_DIR + "zuiltrajanus.png"
                )
            )
            .setContentFile(gEOTRACKER_ASSETS_DIR + "forumtrajanum.html")
            .build()

        val bathsOfDiocletianPos = LatLng(41.90323, 12.49701)
        val bathsOfDiocletian = MarkerBuilder()
            .position(bathsOfDiocletianPos)
            .setTitle("Thermen van Diocletianus")
            .setSubtitle("Grootste en best bewaarde thermen")
            .setMarkerType(activity.getString(R.string.geotracker_marker_icon_name))
            .setIconSize(mARKER_SIZE)
            .setIconAnchor(Property.ICON_ANCHOR_BOTTOM)
            .setImages(
                listOf(
                    gEOTRACKER_ASSETS_DIR + "thermendiocletianus0.jpg",
                    gEOTRACKER_ASSETS_DIR + "thermendiocletianus1.jpg"
                )
            )
            .setContentFile(gEOTRACKER_ASSETS_DIR + "thermendiocletianus.html")
            .build()

        val muurHadrianusPos = LatLng(54.99648, -1.78658)
        val muurHadrianus = MarkerBuilder()
            .position(muurHadrianusPos)
            .setTitle("Muur van Hadrianus")
            .setSubtitle("Verdedigingslinie van keizer Hadrianus")
            .setMarkerType(activity.getString(R.string.geotracker_marker_icon_name))
            .setIconSize(mARKER_SIZE)
            .setIconAnchor(Property.ICON_ANCHOR_BOTTOM)
            .setImages(
                listOf(
                    gEOTRACKER_ASSETS_DIR + "hadrianusmuur.jpg"
                )
            )
            .setContentFile(gEOTRACKER_ASSETS_DIR + "hadriaansemuur.html")
            .build()

        val mausoleumHadrianusPos = LatLng(41.90304, 12.46634)
        val mausoleumHadrianus = MarkerBuilder()
            .position(mausoleumHadrianusPos)
            .setTitle("Mausoleum van Hadrianus")
            .setSubtitle("Tegenwoordig de Engelenburcht")
            .setMarkerType(activity.getString(R.string.geotracker_marker_icon_name))
            .setIconSize(mARKER_SIZE)
            .setIconAnchor(Property.ICON_ANCHOR_BOTTOM)
            .setImages(
                listOf(
                    gEOTRACKER_ASSETS_DIR + "mausoleumvanhadrianus0.jpg",
                    gEOTRACKER_ASSETS_DIR + "mausoleumvanhadrianus1.jpg"
                )
            )
            .setContentFile(gEOTRACKER_ASSETS_DIR + "mausoleumhadrianus.html")
            .build()

        val templeOfVenusAndRomePos = LatLng(41.89082, 12.48999)
        val templeOfVenusAndRome = MarkerBuilder()
            .position(templeOfVenusAndRomePos)
            .setTitle("Tempel van Venus en Roma")
            .setSubtitle("Venus Felix en Roma Aeterna")
            .setMarkerType(activity.getString(R.string.geotracker_marker_icon_name))
            .setIconSize(mARKER_SIZE)
            .setIconAnchor(Property.ICON_ANCHOR_BOTTOM)
            .setImages(
                listOf(
                    gEOTRACKER_ASSETS_DIR + "tempelvenusroma0.jpg",
                    gEOTRACKER_ASSETS_DIR + "tempelvenusroma1.jpg"
                )
            )
            .setContentFile(gEOTRACKER_ASSETS_DIR + "tempelvenusroma.html")
            .build()



        return listOf(
            forumromanum,
            cloacamaxima,
            servianWall,
            forumboarium,
            forumjulium,
            largoDiTorreArgentina,
            tibereiland,
            theatreOfMarcellus,
            forumAugustum,
            araPacis,
            mausoleumOfAugustus,
            templeOfAgrippa,
            domusAurea,
            hotel,
            colosseum,
            forumTrajanum,
            muurHadrianus,
            bathsOfDiocletian,
            mausoleumHadrianus,
            templeOfVenusAndRome
        )
    }

    fun addImageToStyle(style: Style, imageName: String, resId: Int) {
        val image = ResourcesCompat.getDrawable(activity.resources, resId, null)
        val bitmap = getBitmapFromVectorDrawable(image!!)
        style.addImage(
            imageName,
            bitmap,
            false
        )
    }

    private fun getBitmapFromVectorDrawable(drawable: Drawable): Bitmap {
        val bitmap = Bitmap.createBitmap(
            drawable.intrinsicWidth,
            drawable.intrinsicHeight, Bitmap.Config.ARGB_8888
        )
        val canvas = Canvas(bitmap)
        drawable.setBounds(0, 0, canvas.width, canvas.height)
        drawable.draw(canvas)

        return bitmap
    }
}

