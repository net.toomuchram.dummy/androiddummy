package net.toomuchram.dummy.ui.login

/**
 * User details post authentication that is exposed to the UI
 */
data class LoggedInUserView(
    val username: String,
    val sessionId: String,
    val admin: Boolean
    //... other data fields that may be accessible to the UI
)
