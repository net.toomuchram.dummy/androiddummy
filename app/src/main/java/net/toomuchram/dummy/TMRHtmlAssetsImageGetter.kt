package net.toomuchram.dummy

/*
 * Copyright (C) 2016 Daniel Passos <daniel@passos.me>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


import android.content.Context
import android.graphics.drawable.Drawable
import android.text.Html
import android.widget.TextView

import java.io.IOException
import kotlin.math.roundToInt

/**
 * Assets Image Getter
 *
 * Load image from assets folder
 *
 * @author [Daniel Passos](mailto:daniel@passos.me)
 */
class TMRHtmlAssetsImageGetter(private val textView: TextView) : Html.ImageGetter {

    private val context: Context = textView.context

    override fun getDrawable(source: String): Drawable? {

        return try {
            val inputStream = context.assets.open(source)
            val d = Drawable.createFromStream(inputStream, null)

            val multiplier = textView.width.div(d.intrinsicWidth.toFloat())
            val height = multiplier*d.intrinsicHeight


            //Small modification: set width to textview width
            d.setBounds(0, 0, textView.width, height.roundToInt())
            d
        } catch (e: IOException) {
            // prevent a crash if the resource still can't be found
            println("source could not be found: $source")
            null
        }

    }

}
