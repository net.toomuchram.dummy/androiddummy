package net.toomuchram.dummy

import android.app.Activity
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.view.ContextThemeWrapper
import androidx.preference.PreferenceFragmentCompat
import androidx.preference.Preference
import androidx.preference.PreferenceManager
import androidx.preference.SwitchPreferenceCompat


class SettingsActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.settings_activity)
        supportFragmentManager
            .beginTransaction()
            .replace(R.id.settings, SettingsFragment(this))
            .commit()
        supportActionBar?.setDisplayHomeAsUpEnabled(true)


    }

    class SettingsFragment(private val activity: Activity) : PreferenceFragmentCompat() {
        override fun onCreatePreferences(
            savedInstanceState: Bundle?,
            rootKey: String?
        ) {
            setPreferencesFromResource(R.xml.root_preferences, rootKey)

            val appUtils = AppUtilities(activity)

            val logoutButton = findPreference<Preference>(getString(R.string.settings_logout_key))
            logoutButton?.onPreferenceClickListener = Preference.OnPreferenceClickListener {
                //logout
                AlertDialog.Builder(ContextThemeWrapper(activity, R.style.AlertTheme))
                    //set title
                    .setTitle("Weet je het zeker?")
                    //set message
                    .setMessage("Je zal worden uitgelogd van de Romereis-app als je op 'Ja' drukt.")
                    //set positive button
                    .setPositiveButton("Ja"
                    ) { _, _ ->
                        appUtils.logOut(activity)
                    }
                    .setNegativeButton("Nee", null)
                    .show()
                true
            }

            val downloadMapsButton = findPreference<SwitchPreferenceCompat>(getString(R.string.settings_offline_maps_key))
            downloadMapsButton?.onPreferenceChangeListener = Preference.OnPreferenceChangeListener { preference, newValue ->
                if(newValue == true) {
                    appUtils.downloadRomeMap()
                }
                else {
                    appUtils.deleteRomeMap()
                }
                true
            }
        }
    }
}