package net.toomuchram.dummy

import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import com.google.gson.Gson
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class FCMService : FirebaseMessagingService() {


    /**
     * Called if InstanceID token is updated. This may occur if the security of
     * the previous token had been compromised. Note that this is called when the InstanceID token
     * is initially generated so this is where you would retrieve the token.
     */
    override fun onNewToken(token: String) {

        val notifUtils = NotificationUtilities(applicationContext)
        val appUtils = AppUtilities(applicationContext)

        print("New Firebase token: $token")

        // If you want to send messages to this application instance or
        // manage this apps subscriptions on the server side, send the
        // Instance ID token to your app server.

        val sessionId = AccountUtilities(applicationContext).sessionId
        if (sessionId != null) {
            CoroutineScope(Dispatchers.IO).launch {
                val resp =
                    appUtils.sendFirebaseTokenToServerAsync(token, sessionId)
                        .await()
                if (!resp) {
                    notifUtils.notifyNotLoggedIn()
                }
            }
        } else {
            notifUtils.notifyNotLoggedIn()
        }
    }

    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        val notifUtils = NotificationUtilities(applicationContext)

        if (remoteMessage.data["chat"] != null) {
            // Don't send a chat notification when the app is opened
            return
        }

        remoteMessage.notification?.let {msg ->
            if (msg.title != null && msg.body != null) {
                with(NotificationManagerCompat.from(applicationContext)) {
                    notify(
                        notifUtils.createId(), notifUtils.buildNotif(
                            getString(R.string.announcements_notificationchannel_id),
                            R.drawable.ic_announcement_white_24dp,
                            msg.title!!,
                            msg.body!!,
                            NotificationCompat.PRIORITY_HIGH
                        )
                    )
                }
            }
        }
    }


}