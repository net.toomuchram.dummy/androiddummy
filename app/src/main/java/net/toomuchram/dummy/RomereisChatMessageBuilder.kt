package net.toomuchram.dummy

import android.app.Activity
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Color
import android.graphics.Point
import android.view.Gravity
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import androidx.annotation.Dimension
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import org.sufficientlysecure.htmltextview.HtmlTextView


class RomereisChatMessageBuilder(
    private val activity: Activity,
    chatmessage: ChatActivity.ChatMessage,
    private val photoboardMode: Boolean = false
) {
    var withHeader: Boolean = false

    private val username: String = chatmessage.user
    private val timestamp: String = chatmessage.timestamp
    private val imageURL: String? = if (chatmessage.imgpath != "") {
        chatmessage.imgpath
    } else {
        null
    }
    private val message: String = chatmessage.message

    init {
        if (photoboardMode) {
            withHeader = false
        }
    }

    @JvmName("setheadervisible")
    fun setHeaderVisible(withHeader: Boolean): RomereisChatMessageBuilder {
        if (!photoboardMode) {
            this.withHeader = withHeader
        }
        return this
    }

    fun build(): LinearLayout {

        if (photoboardMode) {
            withHeader = false
        }


        val messageContainer = LinearLayout(activity)
        messageContainer.orientation = LinearLayout.VERTICAL

        val separator = View(activity)
        val separatorParams = LinearLayout.LayoutParams(
            LinearLayout.LayoutParams.MATCH_PARENT,
            1
        )
        separator.layoutParams = separatorParams
        separator.setBackgroundColor(Color.parseColor("#3f4246"))

        if(withHeader) {
            messageContainer.addView(separator)
            messageContainer.addView(createHeader())
        }
        if(message != "" && !photoboardMode) {
            messageContainer.addView(createMessage())
        }

        if(imageURL != null) {
            messageContainer.addView(createImage())
        }


        return messageContainer
    }

    private fun createHeader(): HtmlTextView {
        val header = HtmlTextView(activity)
        val headerLayoutParams = LinearLayout.LayoutParams(
            LinearLayout.LayoutParams.MATCH_PARENT,
            LinearLayout.LayoutParams.WRAP_CONTENT
        )
        headerLayoutParams.setMargins(0, 10, 0, 0)
        val userAndTime = "<b>" +
                username +
                "</b> <i><small><small>" +
                timestamp +
                "</small></small></i>"
        header.setHtml(userAndTime)
        header.layoutParams = headerLayoutParams
        return header
    }
    private fun createMessage(): HtmlTextView {
        val messageText = HtmlTextView(activity)
        val messageTextParams = LinearLayout.LayoutParams(
            LinearLayout.LayoutParams.MATCH_PARENT,
            LinearLayout.LayoutParams.WRAP_CONTENT
        )
        messageText.setTextSize(Dimension.SP, 17F)
        messageTextParams.setMargins(0, 5, 0, 20)
        messageText.layoutParams = messageTextParams
        messageText.setHtml(message)
        return messageText
    }
    private fun calculateBitmapHeight(bitmap: Bitmap): Int {
        val mDisplay = activity.windowManager.defaultDisplay
        val size = Point()
        mDisplay.getSize(size)
        return if (bitmap.height > (size.x / 2)) {
            (size.x / 2)
        } else {
            bitmap.height
        }
    }
    private fun createImage(): ImageView? {
        if(imageURL == null) {
            return null
        }
        val img = ImageView(activity)
        val appUtils = AppUtilities(activity)

        val loadingImage = BitmapFactory.decodeStream(
            activity.assets.open("loading.webp")
        )
        img.setImageBitmap(loadingImage)


        CoroutineScope(Dispatchers.Main).launch {
            val bm = appUtils.getBitmapFromURLAsync(imageURL).await()
            if (bm != null) {
                img.setImageBitmap(bm)

                val params: LinearLayout.LayoutParams = if(!photoboardMode) {
                    LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.WRAP_CONTENT,
                        calculateBitmapHeight(bm)
                    )
                } else {
                    LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.MATCH_PARENT,
                        LinearLayout.LayoutParams.WRAP_CONTENT
                    )
                }
                params.setMargins(0, 10, 0, 20)
                params.gravity = Gravity.START

                img.layoutParams = params
                img.adjustViewBounds = true

                img.setOnClickListener {
                    if(!photoboardMode) {
                        appUtils.enlargeImage(bm, activity)
                    } else {
                        val intent = Intent(activity, ImageActivity::class.java)
                        intent.putExtra("imageURL", imageURL)
                        activity.startActivity(intent)
                    }
                }
                //scrollView.fullScroll(ScrollView.FOCUS_DOWN)
            }

        }
        return img
    }


}