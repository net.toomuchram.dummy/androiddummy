package net.toomuchram.dummy

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Build
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import net.toomuchram.dummy.ui.login.UsernameActivity
import java.text.SimpleDateFormat
import java.util.*
import android.media.AudioAttributes
import kotlinx.coroutines.launch

class NotificationUtilities(private val c: Context){

    private val networkScope = CoroutineScope(Dispatchers.IO)

    fun createNotificationChannel(name: String, desc: String, id: String, importance: Int) {
        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val channel = NotificationChannel(id, name, importance).apply {
                description = desc
            }
            val audioAttributes = AudioAttributes.Builder()
                .setContentType(AudioAttributes.CONTENT_TYPE_SONIFICATION)
                .setUsage(AudioAttributes.USAGE_NOTIFICATION_RINGTONE)
                .build()
            channel.setSound(Uri.parse("android.resource://${c.packageName}/" + R.raw.message), audioAttributes)
            // Register the channel with the system
            val notificationManager: NotificationManager =
                c.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            notificationManager.createNotificationChannel(channel)
        }
    }

    fun buildNotif(channel: String, icon: Int, title: String, desc: String, priority: Int): Notification {
        val intent = Intent(c, MainActivity::class.java)
        val pendingIntent = PendingIntent.getActivity(
            c,
            5,
            intent,
            0
        )

        return NotificationCompat.Builder(c, channel)
            .setSmallIcon(icon)
            .setContentTitle(title)
            .setContentText(desc)
            .setPriority(priority)
            .setVisibility(NotificationCompat.VISIBILITY_PUBLIC)
            .setContentIntent(pendingIntent).build()
    }

    fun notifyNotLoggedIn(){
        val intent = Intent(c, UsernameActivity::class.java)
        val pendingIntent = PendingIntent.getActivity(
            c,
            0,
            intent,
            PendingIntent.FLAG_ONE_SHOT
        )
        val notification : Notification
        notification = NotificationCompat.Builder(
            c,
            c.getString(R.string.announcements_notificationchannel_id)
        )
            .setSmallIcon(R.drawable.ic_error_outline_white_24dp)
            .setContentTitle(c.getString(R.string.notif_not_logged_in_title))
            .setContentText(c.getString(R.string.notif_not_logged_in_desc))
            .setPriority(NotificationCompat.PRIORITY_HIGH)
            .setVisibility(NotificationCompat.VISIBILITY_PUBLIC)
            .setContentIntent(pendingIntent).build()
        with(NotificationManagerCompat.from(c)) {
            notify(1337, notification)
        }
    }

    fun notifyMultipleClients(){
        with(NotificationManagerCompat.from(c)) {
            notify(421, buildNotif(
                c.getString(R.string.announcements_notificationchannel_id),
                R.drawable.ic_error_outline_white_24dp,
                c.getString(R.string.notif_multipledevices_title),
                c.getString(R.string.notif_multipledevices_desc),
                NotificationCompat.PRIORITY_DEFAULT))
        }
    }

    fun createNotifChannels() = CoroutineScope(Dispatchers.IO).launch {
        createAnnouncementsChannel()
        createChatChannel()
    }

    private fun createAnnouncementsChannel(){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O){
            createNotificationChannel(
                c.getString(R.string.announcements_notificationchannel_name),
                c.getString(R.string.announcements_notificationchannel_desc),
                c.getString(R.string.announcements_notificationchannel_id),
                5
            )
        }
    }

    private fun createChatChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O){
            createNotificationChannel(
                c.getString(R.string.chat_notificationchannel_name),
                c.getString(R.string.chat_notificationchannel_desc),
                c.getString(R.string.chat_notificationchannel_id),
                3
            )
        }
    }

    fun getPastNotificationsAsync(sessionId: String) = networkScope.async {
        return@async try{
            val resp = khttp.get(
                url = c.getString(R.string.PHP_server_IP) +
                        "/getAnnouncements",
                params = mapOf("sessionId" to sessionId)
            ).text
            if(
                resp == "ERR_NO_SUCH_SESSION" ||
                resp == "ERR_NO_SUCH_USER" ||
                resp == "ERR_MISSING_SESSIONID"
            ) {
                return@async null
            }
            resp
        }
        catch (e: Exception){
            e.printStackTrace()
            null
        }
    }

    fun createId(): Int {
        val now = Date()
        return Integer.parseInt(SimpleDateFormat("ddHHmmss", Locale.UK).format(now))
    }
}