package net.toomuchram.dummy

import android.content.ContentValues
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity

import kotlinx.android.synthetic.main.app_bar_main.*
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Build
import android.provider.MediaStore
import android.view.View
import androidx.core.view.drawToBitmap
import androidx.core.view.isVisible
import com.github.chrisbanes.photoview.PhotoView
import com.google.android.material.floatingactionbutton.FloatingActionButton
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch


class ImageActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_image)
        setSupportActionBar(toolbar)


        val imageView = findViewById<View>(R.id.mainimageview) as PhotoView
        if (intent.hasExtra("imagepath")) {
            val imagepath = intent.getStringExtra("imagepath")
            val bitmap = BitmapFactory
                .decodeStream(assets.open(imagepath!!))

            imageView.setImageBitmap(bitmap)

        } else if (intent.hasExtra("imageURL")) {
            val assetManager = assets
            val bitmap = BitmapFactory
                .decodeStream(assetManager.open("loading.webp"))
            imageView.setImageBitmap(bitmap)
            val appUtils = AppUtilities(this)
            CoroutineScope(Dispatchers.Main).launch {
                val bm = appUtils.getBitmapFromURLAsync(
                    intent.getStringExtra("imageURL")!!
                ).await()
                if (bm != null){
                    imageView.setImageBitmap(bm)
                    val downloadButton = findViewById<FloatingActionButton>(R.id.downloadButton)
                    downloadButton.isVisible = true
                    downloadButton.setOnClickListener {
                        val launchBrowser = Intent(Intent.ACTION_VIEW, Uri.parse(intent.getStringExtra("imageURL")!!))
                        startActivity(launchBrowser)
                    }
                }

            }
        } else if (intent.hasExtra("bitmap")) {
            val byteArray = intent.getByteArrayExtra("bitmap")
            val bitmap = BitmapFactory.decodeByteArray(byteArray, 0, byteArray!!.size)
            imageView.setImageBitmap(bitmap)
        }


    }

}
