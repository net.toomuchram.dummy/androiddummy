package net.toomuchram.dummy

import android.content.*
import androidx.work.Worker
import androidx.work.WorkerParameters
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class RomereisLocationWorker(appContext: Context, workerParams: WorkerParameters) : Worker(appContext, workerParams) {
    override fun doWork(): Result {
        println("LocationWorker was called")
        val context = applicationContext

        val appUtils = AppUtilities(context)
        val notifUtils = NotificationUtilities(context)
        val accountUtils = AccountUtilities(context)
        val locationUtils = LocationUtilities(context)

        //set next alarm
        //appUtils.setAlarm()


        appUtils.getOneTimeLocation { location ->
            if(location != null && locationUtils.getLastLocation() != location){

                locationUtils.setLastLocation(location)

                //Check if the user has already logged in
                val sessionId = accountUtils.sessionId
                if(sessionId == null || sessionId == ""){
                    notifUtils.notifyNotLoggedIn()
                }
                else{
                    //send the location to the server
                    CoroutineScope(Dispatchers.IO).launch{
                        if(appUtils.checkInternetConnectionAsync().await()) {
                            println("sending location to server from alarm service")
                            appUtils.sendLocationToServerAsync(location, sessionId)
                        }
                    }
                }
            }
        }
        return Result.success()
    }

}
