package net.toomuchram.dummy

import android.app.Activity
import android.content.Intent
import android.graphics.BitmapFactory
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import androidx.cardview.widget.CardView
import androidx.core.content.ContextCompat
import com.google.gson.Gson
import com.google.gson.JsonParser
import com.mapbox.mapboxsdk.geometry.LatLng
import com.mapbox.mapboxsdk.plugins.annotation.Symbol
import com.mapbox.mapboxsdk.plugins.annotation.SymbolOptions
import com.mapbox.mapboxsdk.style.layers.Property
import org.sufficientlysecure.htmltextview.HtmlTextView

class MarkerBuilder {

    lateinit var title: String
    var subtitle: String? = null
    var images: List<String>? = null
    var contentFile: String? = null
    var customField: Any? = null
    lateinit var position: LatLng
    var iconSize: Float = 0.6f
    var iconAnchor: String = Property.ICON_ANCHOR_CENTER
    var markerType: String = "marker"

    @JvmName("settitle")
    fun setTitle(title: String): MarkerBuilder{
        this.title = title
        return this
    }

    @JvmName("setsubtitle")
    fun setSubtitle(subtitle: String?): MarkerBuilder {
        this.subtitle = subtitle
        return this
    }

    @JvmName("setimages")
    fun setImages(images: List<String>?): MarkerBuilder {
        this.images = images
        return this
    }

    @JvmName("setcontentfile")
    fun setContentFile(contentFile: String?): MarkerBuilder {
        this.contentFile = contentFile
        return this
    }

    @JvmName("setcustomfield")
    fun setCustomfield(customField: Any?): MarkerBuilder {
        this.customField = customField
        return this
    }

    @JvmName("setlatLng")
    fun setPosition(latLng: LatLng): MarkerBuilder {
        this.position = latLng
        return this
    }
    fun position(latLng: LatLng): MarkerBuilder {
        this.position = latLng
        return this
    }

    @JvmName("seticonsize")
    fun setIconSize(iconSize: Float): MarkerBuilder {
        this.iconSize = iconSize
        return this
    }

    @JvmName("seticonanchor")
    fun setIconAnchor(iconAnchor: String): MarkerBuilder {
        this.iconAnchor = iconAnchor
        return this
    }

    @JvmName("setmarkertype")
    fun setMarkerType(markerType: String): MarkerBuilder {
        this.markerType = markerType
        return this
    }

    fun build(): SymbolOptions {
        val data = JsonTranslator(title, subtitle, images, contentFile, markerType, customField)
        return SymbolOptions()
            .withLatLng(position)
            .withIconImage(markerType)
            .withIconSize(iconSize)
            .withIconAnchor(iconAnchor)
            .withData(JsonParser().parse(Gson().toJson(data)))
    }
}

class JsonTranslator(val title: String,
                     val subtitle: String?,
                     val images: List<String>?,
                     val contentFile: String?,
                     val markerType: String,
                     val customField: Any?)

class DetailViewBuilder(private val detailView: LinearLayout, private val activity: Activity){
    lateinit var title: String
    var subtitle: String? = null
    var images: List<String>? = null
    var contentFile: String? = null

    @JvmName("settitle")
    fun setTitle(title: String): DetailViewBuilder{
        this.title = title
        return this
    }

    @JvmName("setsubtitle")
    fun setSubtitle(subtitle: String?): DetailViewBuilder {
        this.subtitle = subtitle
        return this
    }

    @JvmName("setimages")
    fun setImages(images: List<String>?): DetailViewBuilder {
        this.images = images
        return this
    }

    @JvmName("setcontentfile")
    fun setContentFile(contentFile: String?): DetailViewBuilder {
        this.contentFile = contentFile
        return this
    }

    fun build(){
        setTitleText()
        setImageGallery()
        setContentText()
    }

    private fun setTitleText() {
        val titleElement = detailView.findViewById<HtmlTextView>(R.id.MapDetailViewTitle)
        titleElement.setHtml("")

        var titleText = "<h1>$title</h1>"
        if(subtitle != null) {
            titleText += "<i>$subtitle</i>"
        }
        else {
            titleText += "<br>&nbsp;<br>&nbsp;"
        }
        titleText += "<br>&nbsp;"
        titleElement.setHtml(titleText)
    }

    private fun setImageGallery() {
        val imagesElement = detailView.findViewById<LinearLayout>(R.id.MapDetailViewImageContainer)
        if(imagesElement.childCount > 0) {
            imagesElement.removeAllViews()
        }
        if(images == null) {
            return
        }

        val assetManager = activity.assets

        var i = 0
        for(imagepath in images!!) {
            val bitmap = BitmapFactory.decodeStream(assetManager.open(imagepath))

            val imageElementContainer = CardView(activity)
            imageElementContainer.radius = 20f
            imageElementContainer.setCardBackgroundColor(ContextCompat.getColor(activity, R.color.black_overlay))


            val imageElement = ImageView(activity)
            imageElement.setImageBitmap(bitmap)
            imageElement.scaleType = ImageView.ScaleType.CENTER_CROP
            //imageElement.adjustViewBounds = true

            imageElement.setOnClickListener {
                val intent = Intent(activity, ImageActivity::class.java)
                intent.putExtra("imagepath", imagepath)
                activity.startActivity(intent)
            }
            imageElementContainer.addView(imageElement)
            imagesElement.addView(imageElementContainer)

            val layoutParams = imageElementContainer.layoutParams as ViewGroup.MarginLayoutParams
            if(i == 0) {
                layoutParams.setMargins(20, 20, 10, 10)
            }
            else {
                layoutParams.setMargins(10, 20, 10, 10)
            }
            i++


            imageElement.layoutParams.height = 300
            imageElement.layoutParams.width = 300
        }
    }

    private fun setContentText(){
        val contentElement = detailView.findViewById<HtmlTextView>(R.id.MapDetailViewContent)

        val nothing = activity.assets.open("nothing.html").bufferedReader().use {
            it.readText()
        }
        contentElement.setHtml(nothing)

        if(contentFile == null) {
            //Fill the void
            contentFile = "geotracker/verhaaltjes/lorumipsum.html"
        }
        val html = activity.assets.open(contentFile!!).bufferedReader().use {
            it.readText()
        }
        contentElement.setHtml(html)
    }
}

class MarkerManipulator(val symbol: Symbol) {
    var title: String
    var subtitle: String? = null
    var images: List<String>? = null
    var contentFile: String? = null
    var customField: Any? = null
    var position: LatLng
    var icon: String
    var iconSize: Float = 0.6f
    var iconAnchor: String
    var markerType: String

    init {
        val data = Gson().fromJson(symbol.data!!.asJsonObject, JsonTranslator::class.java)
        this.title = data.title
        this.subtitle = data.subtitle
        this.images = data.images
        this.contentFile = data.contentFile
        this.customField = data.customField
        this.position = symbol.latLng
        this.icon = symbol.iconImage
        this.iconSize = symbol.iconSize
        this.iconAnchor = symbol.iconAnchor
        this.markerType = data.markerType
    }

    fun apply() {
        val data = JsonTranslator(
            title,
            subtitle,
            images,
            contentFile,
            markerType,
            customField
        )
        symbol.latLng = position
        symbol.iconImage = icon
        symbol.iconSize = iconSize
        symbol.data = JsonParser().parse(Gson().toJson(data))
    }

    @JvmName("settitle")
    fun setTitle(title: String): MarkerManipulator {
        this.title = title
        return this
    }

    @JvmName("setsubtitle")
    fun setSubtitle(subtitle: String?): MarkerManipulator {
        this.subtitle = subtitle
        return this
    }

    @JvmName("setimages")
    fun setImages(images: List<String>?): MarkerManipulator {
        this.images = images
        return this
    }

    @JvmName("setcontentfile")
    fun setContentFile(contentFile: String?): MarkerManipulator {
        this.contentFile = contentFile
        return this
    }

    @JvmName("setcustomfield")
    fun setCustomfield(customField: Any?): MarkerManipulator {
        this.customField = customField
        return this
    }

    @JvmName("setlatLng")
    fun setPosition(latLng: LatLng): MarkerManipulator {
        this.position = latLng
        return this
    }

    @JvmName("setmarkertype")
    fun setMarkerType(markerType: String): MarkerManipulator {
        this.markerType = markerType
        return this
    }
    @JvmName("seticon")
    fun setIcon(iconName: String): MarkerManipulator {
        this.icon = iconName
        return this
    }

    @JvmName("seticonsize")
    fun setIconSize(iconSize: Float): MarkerManipulator {
        this.iconSize = iconSize
        return this
    }

    @JvmName("seticonanchor")
    fun setIconAnchor(iconAnchor: String): MarkerManipulator {
        this.iconAnchor = iconAnchor
        return this
    }
}