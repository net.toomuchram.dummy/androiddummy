package net.toomuchram.dummy


import android.annotation.SuppressLint
import android.content.Intent
import android.content.SharedPreferences
import android.os.Build
import android.os.Bundle
import androidx.preference.PreferenceManager
import android.util.DisplayMetrics
import android.view.View
import android.widget.LinearLayout
import android.widget.ScrollView
import android.widget.ShareActionProvider
import androidx.core.content.ContextCompat
import androidx.appcompat.app.AppCompatActivity
import android.widget.Toast
import androidx.annotation.RequiresApi
import com.github.marlonlom.utilities.timeago.TimeAgo
import com.mapbox.android.core.permissions.PermissionsListener
import com.mapbox.android.core.permissions.PermissionsManager
import com.mapbox.mapboxsdk.Mapbox
import com.mapbox.mapboxsdk.geometry.LatLng
import com.mapbox.mapboxsdk.geometry.LatLngBounds
import com.mapbox.mapboxsdk.location.LocationComponentActivationOptions
import com.mapbox.mapboxsdk.location.LocationComponentOptions
import com.mapbox.mapboxsdk.location.modes.CameraMode
import com.mapbox.mapboxsdk.location.modes.RenderMode
import com.mapbox.mapboxsdk.maps.MapboxMap
import com.mapbox.mapboxsdk.maps.OnMapReadyCallback
import com.mapbox.mapboxsdk.maps.Style
import com.mapbox.mapboxsdk.offline.OfflineTilePyramidRegionDefinition
import com.mapbox.mapboxsdk.plugins.annotation.Symbol
import com.mapbox.mapboxsdk.plugins.annotation.SymbolManager
import com.mapbox.mapboxsdk.plugins.offline.model.NotificationOptions
import com.mapbox.mapboxsdk.plugins.offline.model.OfflineDownloadOptions
import com.mapbox.mapboxsdk.plugins.offline.offline.OfflinePlugin
import com.mapbox.mapboxsdk.plugins.offline.utils.OfflineUtils
import com.mapbox.mapboxsdk.style.layers.Property.ICON_ROTATION_ALIGNMENT_VIEWPORT
import com.mapbox.mapboxsdk.style.sources.GeoJsonOptions
import com.sothree.slidinguppanel.SlidingUpPanelLayout
import kotlinx.android.synthetic.main.activity_maps.*
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import org.json.JSONObject
import org.sufficientlysecure.htmltextview.HtmlTextView
import java.time.*
import java.time.format.DateTimeFormatter
import kotlin.collections.HashMap
import kotlin.concurrent.fixedRateTimer


class MapsActivity : AppCompatActivity(), OnMapReadyCallback,
    PermissionsListener {

    private var permissionsManager: PermissionsManager =
        PermissionsManager(this)
    private lateinit var mapboxMap: MapboxMap
    private lateinit var symbolManager: SymbolManager

    private lateinit var utils: MapsActivityUtilities
    private lateinit var accountUtils: AccountUtilities
    private lateinit var appUtils: AppUtilities
    private lateinit var sharedPreferences: SharedPreferences

    private var mapViewDestroyed = false

    private lateinit var slidingLayout: SlidingUpPanelLayout
    //currently opened marker
    private var currentlyOpened: String? = null

    private var markerMap = HashMap<String, Symbol>()
    private lateinit var pointsOfInterest: List<Symbol>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        utils = MapsActivityUtilities(this)
        accountUtils = AccountUtilities(applicationContext)
        appUtils = AppUtilities(applicationContext)


        // Mapbox access token is configured here. This needs to be called either in your application
        // object or in the same activity which contains the mapview.
        Mapbox.getInstance(this, getString(R.string.mapbox_access_token))

        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this)

        // Check if the user has mapbox telemetry enabled
        val telemetryEnabled = sharedPreferences.getBoolean(getString(R.string.settings_mapbox_telemetry_key), true)
        Mapbox.getTelemetry()?.setUserTelemetryRequestState(telemetryEnabled)

        // This contains the MapView in XML and needs to be called after the access token is configured.
        setContentView(R.layout.activity_maps)

        mapView.onCreate(savedInstanceState)
        mapView.getMapAsync(this)
        slidingLayout = findViewById(R.id.sliding_layout)
        slidingLayout.coveredFadeColor = android.R.color.transparent
        //Set a dummy user for the detail view
        //so its anchor point gets set correctly
        updateTimeInDetailView(
            getString(R.string.geotracker_dummy_user),
            getString(R.string.geotracker_dummy_time)
        )
    }


    @SuppressLint("MissingPermission")
    private fun enableLocationComponent(loadedMapStyle: Style) {
        // Check if permissions are enabled and if not request
        if (PermissionsManager.areLocationPermissionsGranted(this)) {

            // Create and customize the LocationComponent's options
            val customLocationComponentOptions =
                LocationComponentOptions.builder(this)
                    .trackingGesturesManagement(true)
                    .accuracyColor(
                        ContextCompat.getColor(
                            this,
                            R.color.mapboxGreen
                        )
                    )
                    .build()

            val locationComponentActivationOptions =
                LocationComponentActivationOptions.builder(this, loadedMapStyle)
                    .locationComponentOptions(customLocationComponentOptions)
                    .build()


            // Get an instance of the LocationComponent and then adjust its settings
            mapboxMap.locationComponent.apply {

                // Activate the LocationComponent with options
                activateLocationComponent(locationComponentActivationOptions)
                // Enable to make the LocationComponent visible
                isLocationComponentEnabled = true

                // Set the LocationComponent's camera mode
                cameraMode = CameraMode.TRACKING

                // Set the LocationComponent's render mode
                renderMode = RenderMode.NORMAL

            }

        } else {
            permissionsManager = PermissionsManager(this)
            permissionsManager.requestLocationPermissions(this)
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        permissionsManager.onRequestPermissionsResult(
            requestCode,
            permissions,
            grantResults
        )
    }

    override fun onExplanationNeeded(permissionsToExplain: List<String>) {
        Toast.makeText(this, "geef je locatie", Toast.LENGTH_LONG).show()
    }

    override fun onPermissionResult(granted: Boolean) {
        if (granted) {
            enableLocationComponent(mapboxMap.style!!)
        } else {
            finish()
        }
    }

    override fun onStart() {
        super.onStart()
        mapView.onStart()
    }

    override fun onResume() {
        super.onResume()
        mapView.onResume()
    }

    override fun onPause() {
        super.onPause()
        mapView.onPause()
    }

    override fun onStop() {
        super.onStop()
        mapView.onStop()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        mapView.onSaveInstanceState(outState)
    }

    override fun onDestroy() {
        super.onDestroy()
        mapView.onDestroy()
        mapViewDestroyed = true

    }

    override fun onLowMemory() {
        super.onLowMemory()
        mapView.onLowMemory()
    }

    override fun onMapReady(mapboxMap: MapboxMap) {
        this.mapboxMap = mapboxMap
        mapboxMap.uiSettings.isAttributionEnabled = false

        val rotateMap = sharedPreferences.getBoolean(getString(R.string.settings_rotate_map_key), true)
        mapboxMap.uiSettings.isRotateGesturesEnabled = rotateMap

        mapboxMap.setStyle(Style.MAPBOX_STREETS) { style ->
            // Map is set up and the style has loaded. Now you can add data or make other map adjustments
            addImagesToStyle(style)

            // create symbol manager
            val geoJsonOptions = GeoJsonOptions().withTolerance(0.4f)
            symbolManager =
                SymbolManager(mapView, mapboxMap, style, null, geoJsonOptions)

            //Enable the location component AFTER the symbol manager is initialised
            //so the location component gets added on top of the markers
            enableLocationComponent(style)

            // Calculate anchor point
            //Set the absolute floating point in dp
            //This is done by taking the DPs and dividing it by the total height of the detailView
            val absoluteDpHeight = 98f
            val absolutePxHeight = appUtils.convertDpToPixel(absoluteDpHeight)
            // The detail view height is set to match_parent, which is equal to the total screen height
            // So we can just take the total screen height.
            val displayMetrics = DisplayMetrics()
            windowManager.defaultDisplay.getMetrics(displayMetrics)

            val anchorPoint = absolutePxHeight / displayMetrics.heightPixels
            slidingLayout.anchorPoint = anchorPoint

            // set non-data-driven properties, such as:
            symbolManager.iconAllowOverlap = true
            symbolManager.iconRotationAlignment =
                ICON_ROTATION_ALIGNMENT_VIEWPORT

            symbolManager.addClickListener {
                if (it.data != null) {
                    onSymbolClicked(it)
                }
            }

            val tempPOIs = mutableListOf<Symbol>()
            for (symboloptions in utils.createMarkers()) {
                val symbol = symbolManager.create(symboloptions)
                tempPOIs.add(symbol)
            }
            pointsOfInterest = tempPOIs
            EventBus.getDefault().register(this)
            //Ask the websocket to ask the server for cached locations
            val serviceIntent =
                Intent(applicationContext, RomereisBGService::class.java)
            serviceIntent.putExtra("action", "getCachedLocations")
            startService(serviceIntent)

            //Set a time to update time ago
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                fixedRateTimer("TimeAgoTimer", false, 0, 30000) {
                    updateTimeAgo()
                }
            }
            slidingLayout.addPanelSlideListener(SlidingListener(this))
        }
    }

    class SlidingListener(private val activity: MapsActivity) :
        SlidingUpPanelLayout.PanelSlideListener {
        override fun onPanelSlide(panel: View?, slideOffset: Float) {
        }

        override fun onPanelStateChanged(
            panel: View,
            previousState: SlidingUpPanelLayout.PanelState,
            newState: SlidingUpPanelLayout.PanelState
        ) {
            if (newState == SlidingUpPanelLayout.PanelState.COLLAPSED || newState == SlidingUpPanelLayout.PanelState.HIDDEN) {
                activity.resetUserIcons()
                activity.currentlyOpened = null
            }
        }
    }

    private fun addImagesToStyle(style: Style) {
        mapboxMap.getStyle(this::addUserImageToStyle)
        mapboxMap.getStyle(this::addUserSelectedImageToStyle)

        mapboxMap.getStyle(this::addMarkerImageToStyle)
        mapboxMap.getStyle(this::addMarkerSelectedImageToStyle)

        mapboxMap.getStyle(this::addHotelImageToStyle)
        mapboxMap.getStyle(this::addHotelSelectedImageToStyle)

        addUserImageToStyle(style)
        addUserSelectedImageToStyle(style)

        addMarkerImageToStyle(style)
        addMarkerSelectedImageToStyle(style)

        addHotelImageToStyle(style)
        addHotelSelectedImageToStyle(style)
    }

    private fun addUserImageToStyle(style: Style) {
        utils.addImageToStyle(
            style,
            getString(R.string.geotracker_user_icon_name),
            R.drawable.ic_account_circle_geotracker
        )
    }

    private fun addUserSelectedImageToStyle(style: Style) {
        utils.addImageToStyle(
            style,
            "${getString(R.string.geotracker_user_icon_name)}Selected",
            R.drawable.ic_account_circle_selected_geotracker
        )
    }

    private fun addMarkerImageToStyle(style: Style) {
        utils.addImageToStyle(
            style,
            getString(R.string.geotracker_marker_icon_name),
            R.drawable.ic_map_marker
        )
    }

    private fun addMarkerSelectedImageToStyle(style: Style) {
        utils.addImageToStyle(
            style,
            "${getString(R.string.geotracker_marker_icon_name)}Selected",
            R.drawable.ic_map_marker_selected
        )
    }

    private fun addHotelImageToStyle(style: Style) {
        utils.addImageToStyle(
            style,
            getString(R.string.geotracker_hotel_icon_name),
            R.drawable.ic_hotel
        )
    }

    private fun addHotelSelectedImageToStyle(style: Style) {
        utils.addImageToStyle(
            style,
            "${getString(R.string.geotracker_hotel_icon_name)}Selected",
            R.drawable.ic_hotel_selected_geotracker
        )
    }

    private fun resetUserIcons(except: Symbol? = null) {
        for ((_, marker) in markerMap) {
            if (marker != except) {
                val markerManipulator = MarkerManipulator(marker)
                markerManipulator.setIcon(markerManipulator.markerType).apply()
                symbolManager.update(marker)
            }
        }
        for (marker in pointsOfInterest) {
            if (marker != except) {
                val markerManipulator = MarkerManipulator(marker)
                markerManipulator.setIcon(markerManipulator.markerType).apply()
                symbolManager.update(marker)
            }
        }
    }

    override fun onBackPressed() {
        when {
            slidingLayout.panelState == SlidingUpPanelLayout.PanelState.EXPANDED -> {
                slidingLayout.panelState =
                    SlidingUpPanelLayout.PanelState.ANCHORED
                //Scroll to top so the view doesn't get glitched out
                findViewById<ScrollView>(R.id.MapDetailScrollView).fullScroll(
                    ScrollView.FOCUS_UP
                )
            }
            slidingLayout.panelState == SlidingUpPanelLayout.PanelState.ANCHORED -> slidingLayout.panelState =
                SlidingUpPanelLayout.PanelState.COLLAPSED
            slidingLayout.panelState == SlidingUpPanelLayout.PanelState.COLLAPSED -> super.onBackPressed()
        }
    }

    private fun onSymbolClicked(symbol: Symbol) {
        val detailView =
            slidingLayout.findViewById<LinearLayout>(R.id.MapDetailView)

        val markerManipulator = MarkerManipulator(symbol)
        if (markerManipulator.title == currentlyOpened) {
            //Deselect the marker
            slidingLayout.panelState = SlidingUpPanelLayout.PanelState.COLLAPSED
            resetUserIcons()
            return
        }

        markerManipulator.setIcon(markerManipulator.markerType + "Selected")
            .apply()
        symbolManager.update(symbol)
        resetUserIcons(symbol)


        DetailViewBuilder(
            detailView,
            this
        )
            .setTitle(markerManipulator.title)
            .setSubtitle(markerManipulator.subtitle)
            .setImages(markerManipulator.images)
            .setContentFile(markerManipulator.contentFile)
            .build()
        slidingLayout.panelState = SlidingUpPanelLayout.PanelState.ANCHORED
        currentlyOpened = markerManipulator.title
    }

    private fun updateTimeInDetailView(user: String, newtime: String) {
        val titleEl =
            slidingLayout.findViewById<HtmlTextView>(R.id.MapDetailViewTitle)
        titleEl.setHtml("<h1>$user</h1><i>$newtime</i><br>&nbsp;<br>&nbsp;")
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onMessageEvent(event: MessageEvent) {
        val message = JSONObject(event.message)
        if (message.getString("channel") == "errors") {
            Toast.makeText(
                this, message.getString("error"),
                Toast.LENGTH_LONG
            ).show()
        }
        if (message.getString("channel") != "locations") {
            return
        }
        if (mapViewDestroyed) {
            return
        }

        val lat = message.getString("lat")
        val lon = message.getString("lon")
        val user = message.getString("user")
        val time = message.getString("time")

        val coordinates = LatLng(lat.toDouble(), lon.toDouble())

        addUserToMap(user, time, coordinates)
    }

    fun addUserToMap(user: String, time: String, position: LatLng) {
        //Create a "time ago" description when the user is on O or newer
        val desc = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val datetime = LocalDateTime.parse(
                time,
                DateTimeFormatter.ISO_OFFSET_DATE_TIME
            )
            val zonedDateTime = datetime.atZone(ZoneId.of("Europe/Amsterdam"))
            val millis = zonedDateTime
                .toInstant()
                .toEpochMilli()
            TimeAgo.using(millis)
        } else {
            time
        }

        val username = accountUtils.username
        if (username == user) {
            //the user already sees themselves on the map
            return
        }

        if (markerMap[user] != null && mapView != null) {
            val marker = markerMap[user]
            val data = marker?.data?.asJsonObject
            if (data != null) {
                MarkerManipulator(marker)
                    .setSubtitle(desc)
                    .setPosition(position)
                    .setCustomfield(time)
                    .apply()
                if (user == currentlyOpened) {
                    updateTimeInDetailView(user, desc)
                }
            }
            symbolManager.update(marker)
        } else {
            val marker = symbolManager.create(
                MarkerBuilder()
                    .setMarkerType("user")
                    .setTitle(user)
                    .setSubtitle(desc)
                    .setPosition(position)
                    .setCustomfield(time)
                    .setIconSize(0.6f)
                    .build()
            )
            markerMap[user] = marker
        }
    }

    @RequiresApi(Build.VERSION_CODES.O)
    fun updateTimeAgo() {
        markerMap.forEach { (user, marker) ->
            val originalTime = MarkerManipulator(marker).customField as String?
            if (originalTime != null) {

                val datetime = LocalDateTime.parse(
                    originalTime,
                    DateTimeFormatter.ISO_OFFSET_DATE_TIME
                )
                val zonedDateTime =
                    datetime.atZone(ZoneId.of("Europe/Amsterdam"))

                val millis = zonedDateTime
                    .toInstant()
                    .toEpochMilli()
                val timeAgo = TimeAgo.using(millis)
                runOnUiThread {
                    MarkerManipulator(marker)
                        .setSubtitle(timeAgo)
                        .apply()
                    symbolManager.update(marker)
                    if (user == currentlyOpened) {
                        updateTimeInDetailView(user, timeAgo)
                    }
                }
            }
        }
    }

}

