package net.toomuchram.dummy

import android.content.Context
import android.location.Location
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat

class RomereisWSUtilities(val c: Context){
    private val notifUtils = NotificationUtilities(c)
    fun sendLocation(location: Location, sessionId: String, mWs: RomereisWebsocket){
        if(mWs.connected && mWs.loggedIn){
            mWs.send("""
                {
                "sessionId": "${sessionId}",
                "lat": "${location.latitude}",
                "lon": "${location.longitude}",
                "channel": "locations"
                }
            """.trimIndent())
        }
    }
}