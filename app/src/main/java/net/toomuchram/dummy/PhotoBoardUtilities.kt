package net.toomuchram.dummy

import android.content.Intent
import android.view.View
import android.widget.LinearLayout
import android.widget.ProgressBar
import androidx.core.view.isVisible
import com.google.gson.Gson
import kotlinx.coroutines.*
import net.toomuchram.dummy.ui.login.UsernameActivity
import org.json.JSONArray

class PhotoBoardUtilities(private val activity: ChatActivity) {


    private val accountUtils = AccountUtilities(activity)
    private val activityScope = CoroutineScope(Dispatchers.Main)

    fun initPhotoBoard() {

        activity.run {
            findViewById<LinearLayout>(R.id.layout_chatbox).isVisible = false
            findViewById<View>(R.id.separator).isVisible = false
        }

        val sessionId = accountUtils.sessionId
        if(sessionId == null) {
            val intent = Intent(activity, UsernameActivity::class.java)
            activity.startActivity(intent)
            return
        }
        activityScope.launch {
            val messageHistory = getMessageHistoryAsync(sessionId)
            if (messageHistory != null) {
                loadMessageHistory(messageHistory)
            }
            activity.run {
                val activityIndicator = findViewById<ProgressBar>(R.id.messageProgressBar)
                activityIndicator.visibility = View.GONE
            }
        }
    }

    private suspend fun getMessageHistoryAsync(sessionId: String): List<ChatActivity.ChatMessage>? {
        return withContext(Dispatchers.IO) {
            return@withContext try {
                val resp = khttp.get(
                    url = activity.getString(R.string.PHP_server_IP) +
                            "/getMessages?sessionId=$sessionId&type=photoboard"
                ).text
                val gson = Gson()
                val messageList = mutableListOf<ChatActivity.ChatMessage>()
                val parsedJsonArray = JSONArray(resp)
                for (i in 0 until parsedJsonArray.length()) {
                    val obj = parsedJsonArray.getJSONObject(i)
                    messageList.add(
                        gson.fromJson(
                            obj.toString(),
                            ChatActivity.ChatMessage::class.java
                        )
                    )
                }
                messageList
            } catch (e: Exception) {
                e.printStackTrace()
                null
            }
        }
    }

    fun loadMessageHistory(messageHistory: List<ChatActivity.ChatMessage>) {
        for (i in 0 until messageHistory.count()) {
            activity.addMessage(messageHistory[i], withHeader = false)
        }

    }
}