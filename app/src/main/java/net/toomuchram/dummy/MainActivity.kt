package net.toomuchram.dummy

import android.content.Context
import android.os.Bundle
import androidx.core.view.GravityCompat
import androidx.appcompat.app.ActionBarDrawerToggle
import android.view.MenuItem
import androidx.drawerlayout.widget.DrawerLayout
import com.google.android.material.navigation.NavigationView
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import android.content.Intent
import android.location.Location
import android.widget.Toast
import kotlinx.coroutines.*
import android.net.Uri
import android.os.Handler
import android.util.Log
import android.view.View
import android.widget.ProgressBar
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.view.ContextThemeWrapper
import androidx.preference.PreferenceManager
import java.util.*


class MainActivity : AppCompatActivity(),
    NavigationView.OnNavigationItemSelectedListener {

    private lateinit var utils: MainActivityUtilities
    private lateinit var accountUtils: AccountUtilities
    private lateinit var appUtils: AppUtilities
    private lateinit var notifUtils: NotificationUtilities

    private lateinit var runnable: Runnable

    private val activityScope = CoroutineScope(Dispatchers.Main)

    override fun onCreate(savedInstanceState: Bundle?) {
        /*
            native code
         */
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val toolbar: Toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)

        val drawerLayout: DrawerLayout = findViewById(R.id.drawer_layout)
        val navView: NavigationView = findViewById(R.id.nav_view)
        val toggle = ActionBarDrawerToggle(
            this, drawerLayout,
            toolbar,
            R.string.navigation_drawer_open,
            R.string.navigation_drawer_close
        )
        drawerLayout.addDrawerListener(toggle)
        toggle.syncState()

        navView.setNavigationItemSelectedListener(this)

        /*
            custom code
         */

        utils = MainActivityUtilities(this)
        accountUtils = AccountUtilities(applicationContext)
        appUtils = AppUtilities(applicationContext)
        notifUtils = NotificationUtilities(applicationContext)

        LocationUtilities(applicationContext).resetLastLocation()

        this.title = "Romereis 2020"

        //check if the Romereis already begun
        val millisRemaining = appUtils.getRemainingMillis()
        if (millisRemaining < 0) {
            //Hide countdown entry
            val menu = findViewById<NavigationView>(R.id.nav_view)
            val item = menu.menu.findItem(R.id.nav_countdown)
            item.isVisible = false
        }


        //create notification channel
        //it's also created in the bgservice, but do it just to be sure
        notifUtils.createNotifChannels()

        val sessionId = accountUtils.sessionId
        if (sessionId == null || sessionId == "") {
            utils.loadLoginScreen()
            return
        }
        defineRunnable(sessionId)

        if (!appUtils.verifyLocationPermissions()) {
            appUtils.requestLocationPermissions(this)
        } else {
            runnable.run()
        }

    }

    private fun defineRunnable(sessionId: String) {
        runnable = Runnable {
            activityScope.launch {
                val (sessionValidity, admin) = utils.checkSessionValidityAsync(
                    sessionId
                )
                if (sessionValidity) {

                    if (admin != null) {
                        val sharedPref = this@MainActivity.getSharedPreferences(
                            getString(R.string.prefs_file_key),
                            Context.MODE_PRIVATE
                        )
                        with(sharedPref.edit()) {
                            putBoolean("admin", admin)
                            apply()
                        }
                    }

                    accountUtils = AccountUtilities(applicationContext)

                    appUtils.getAndSaveHtmlContent("programme", sessionId)
                    appUtils.getAndSaveHtmlContent("info", sessionId)
                    appUtils.getAndSaveHtmlContent("teams", sessionId)

                    utils.loadHomeScreen(sessionId)


                    appUtils.setAlarm()

                    // Putting it in a try block because android seems to start
                    // the app in background, which crashes the app
                    try {
                        //Close all remaining services before starting a new one
                        stopService(
                            Intent(
                                applicationContext,
                                RomereisBGService::class.java
                            )
                        )
                        startService(
                            Intent(
                                applicationContext,
                                RomereisBGService::class.java
                            )
                        )
                        if (!accountUtils.admin) {
                            utils.hideAdminAreas()
                        }
                    } catch (e: Exception) {
                        println("Exception during service starting")
                        e.printStackTrace()
                    }





                    val sharedPreferences =
                        PreferenceManager.getDefaultSharedPreferences(this@MainActivity)
                    if (sharedPreferences.getBoolean(
                            getString(R.string.settings_offline_maps_key),
                            false
                        )
                    ) {
                        appUtils.downloadRomeMap()
                    }

                    utils.getAndSendFirebaseToken()
                } else {
                    utils.loadLoginScreen()
                }
            }
        }
    }

    override fun onBackPressed() {
        val drawerLayout: DrawerLayout = findViewById(R.id.drawer_layout)
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START)
        } else {
            //super.onBackPressed()
            drawerLayout.openDrawer(GravityCompat.START)
        }
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        //Hide the spinner just to be sure
        val spinner = findViewById<ProgressBar>(R.id.progressBar1)
        spinner.visibility = View.GONE
        // Handle navigation view item clicks here.
        when (item.itemId) {
            R.id.nav_geotracker -> {

                if (appUtils.verifyLocationPermissions()) {

                    val intent =
                        Intent(applicationContext, MapsActivity::class.java)
                    startActivity(intent)
                } else {

                    Toast.makeText(
                        applicationContext,
                        "Geef eerst locatierechten",
                        Toast.LENGTH_LONG
                    ).show()
                }

            }
            R.id.nav_feed -> {
                val sessionId = accountUtils.sessionId
                title = getString(R.string.nav_feed)
                if (sessionId == null || sessionId == "") {
                    utils.loadLoginScreen()
                } else {
                    activityScope.launch {
                        val feed = utils.feedToHTML(sessionId)
                        if (feed != null) {
                            utils.loadMainWebview(feed)
                        } else {
                            utils.loadMainWebview(
                                "Er is iets fout gegaan. Controleer of je een werkende internetverbinding hebt."
                            )
                        }
                    }

                }

            }
            R.id.nav_send_notif -> {
                val intent =
                    Intent(applicationContext, SendNotifActivity::class.java)
                startActivity(intent)
            }
            R.id.nav_chat -> {
                val intent =
                    Intent(applicationContext, ChatActivity::class.java)
                startActivity(intent)
            }
            R.id.nav_adminchat -> {
                val intent =
                    Intent(applicationContext, ChatActivity::class.java)
                intent.putExtra("adminchat", true)
                startActivity(intent)
            }
            R.id.nav_privacy_policy -> {
                val launchBrowser = Intent(
                    Intent.ACTION_VIEW,
                    Uri.parse(getString(R.string.privacy_policy_url))
                )
                startActivity(launchBrowser)
            }
            R.id.nav_settings -> {
                val intent =
                    Intent(applicationContext, SettingsActivity::class.java)
                startActivity(intent)
            }
            R.id.nav_countdown -> {
                val intent =
                    Intent(applicationContext, CountdownActivity::class.java)
                startActivity(intent)
            }
            R.id.nav_photoboard -> {
                val intent =
                    Intent(applicationContext, ChatActivity::class.java)
                intent.putExtra("photoboard", true)
                startActivity(intent)
            }
            R.id.nav_audio -> {
                val intent =
                    Intent(applicationContext, AudioPlayerActivity::class.java)
                startActivity(intent)
            }
            else -> {
                utils.loadNormal(item)
            }
        }


        val drawerLayout: DrawerLayout = findViewById(R.id.drawer_layout)
        drawerLayout.closeDrawer(GravityCompat.START)
        return true
    }

    override fun onDestroy() {
        super.onDestroy()
        //Stop the service
        stopService(Intent(applicationContext, RomereisBGService::class.java))
    }


    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        Handler().postDelayed(Runnable {
            runnable.run()
        }, 500)
    }

    override fun onActivityResult(
        requestCode: Int,
        resultCode: Int,
        data: Intent?
    ) {
        super.onActivityResult(requestCode, resultCode, data)
        accountUtils = AccountUtilities(applicationContext)
        val sessionId = accountUtils.sessionId!!
        defineRunnable(sessionId)
        if (requestCode == 101 /* Request code for login activity */) {
            //run the normal stuff
            if (!appUtils.verifyLocationPermissions()) {
                appUtils.requestLocationPermissions(this)
            } else {
                Handler().postDelayed(Runnable {
                    runnable.run()
                }, 500)
            }
        }
    }
}
