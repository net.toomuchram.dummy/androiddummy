package net.toomuchram.dummy

import android.content.Context
import android.content.SharedPreferences
import android.location.Location

class LocationUtilities(c: Context) {

    private val sharedPref: SharedPreferences =
        c.getSharedPreferences(c.getString(R.string.prefs_file_key), Context.MODE_PRIVATE)

    fun getLastLocation(): Location{
        val lat = sharedPref.getFloat("lat", 0.0F)
        val lon = sharedPref.getFloat("lon", 0.0F)
        val location = Location("")
        location.latitude = lat.toDouble()
        location.longitude = lon.toDouble()
        return location
    }

    fun setLastLocation(location: Location){
        val editor = sharedPref.edit()
        editor.putFloat("lat", location.latitude.toFloat())
        editor.putFloat("lon", location.longitude.toFloat())
        editor.apply()
    }

    fun resetLastLocation(){
        val location = Location("")
        location.latitude = 0.0
        location.longitude = 0.0
        setLastLocation(location)
    }
}