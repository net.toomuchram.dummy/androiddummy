package net.toomuchram.dummy

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.Intent.FLAG_ACTIVITY_NEW_TASK
import android.os.Build
import android.util.Log
import net.toomuchram.dummy.ui.login.UsernameActivity
import org.sufficientlysecure.htmltextview.HtmlAssetsImageGetter
import org.sufficientlysecure.htmltextview.HtmlTextView
import android.widget.Toast
import android.view.MenuItem
import android.view.View
import android.widget.ProgressBar
import com.google.android.material.navigation.NavigationView
import kotlinx.coroutines.*
import net.toomuchram.dummy.data.LoginDataSource
import net.toomuchram.dummy.data.LoginRepository
import org.json.JSONArray


//c means applicationContext
class MainActivityUtilities(
    private val activity: Activity
) {

    private val activityScope = CoroutineScope(Dispatchers.Main)
    private val networkScope = CoroutineScope(Dispatchers.IO)


    fun loadImage(filename: String) {
        val intent = Intent(activity, ImageActivity::class.java)
        intent.putExtra("imagepath", filename)
        activity.startActivity(intent)
    }

    private fun loadMainWebviewFromFile(filename: String) {

        var html = activity.assets.open(filename).bufferedReader().use {
            it.readText()
        }
        html = html.replace("{{{version}}}", BuildConfig.VERSION_NAME)
        loadMainWebview(html)
    }

    fun loadMainWebview(html: String) {
        val htmlTextView = activity.findViewById<HtmlTextView>(R.id.mainwebview)
        // Load the formatted HTML into the view.
        htmlTextView.setHtml(html, TMRHtmlAssetsImageGetter(htmlTextView))
    }

    private fun loadAudio(filename: String, audioname: String) {
        val intent = Intent(activity, AudioPlayerActivity::class.java)
        intent.putExtra("audiofile", filename)
        intent.putExtra("audioname", audioname)
        activity.startActivity(intent)
    }

    fun loadResource(filename: String) {

        if(filename == "info.html" || filename == "programme.html" || filename == "teams.html") {
            val appUtils = AppUtilities(activity)

            try {
                loadMainWebview(appUtils.readFromFile(filename).toString())
            }
            catch(e: Exception) {
                e.printStackTrace()
                loadMainWebview("$filename has not been downloaded yet")
            }
            return
        }

        val extension = filename.substring(filename.lastIndexOf('.') + 1)
        if (extension == "html" || extension == "htm") {
            loadMainWebviewFromFile(filename)
        } else if (extension == "png" || extension == "jpg" || extension == "jpeg" || extension == "webp") {
            loadImage(filename)
        }

    }


    suspend fun feedToHTML(sessionId: String): String? {

        val notifUtils = NotificationUtilities(activity)

        val spinner = activity.findViewById<ProgressBar>(R.id.progressBar1)
        spinner.visibility = View.VISIBLE

        loadMainWebview(" ")

        return withContext(Dispatchers.Main) {
            val feed = notifUtils.getPastNotificationsAsync(sessionId)
            if (feed.await() != null) {
                try {
                    val arr = JSONArray(feed.await())

                    var html = """
                <!DOCTYPE html>
                <html>
                    <body>
            """.trimIndent()

                    for (i in 0 until arr.length()) {
                        val obj = arr.getJSONObject(i)

                        html += "${obj.getString("notiftime")}: ${obj.getString("message")}<br>"
                    }
                    if (arr.length() == 0) {
                        html += "Er zijn geen notificaties."
                    }

                    html += """
                    </body>
                </html>
            """.trimIndent()
                    spinner.visibility = View.GONE
                    return@withContext html
                }
                catch (e: Exception) {
                    spinner.visibility = View.GONE
                    e.printStackTrace()
                    return@withContext null
                }
            } else {
                spinner.visibility = View.GONE
            }
            return@withContext null
        }
    }

    fun loadNormal(item: MenuItem) {
        //Look up the itemID
        var itemid = activity.resources.getResourceName(item.itemId).toString()
        itemid = itemid.substring(itemid.indexOf('/') + 1)

        //retrieve the filename and load the file
        val filename = activity.getString(
            activity.resources.getIdentifier(
                itemid + "_filename",
                "string", activity.packageName
            )
        )
        val extension = filename.substring(
            filename.lastIndexOf('.') + 1
        )

        loadResource(filename)

        if (extension == "html" || extension == "htm") {
            activity.title = item.title.toString()
        }
    }

    //this function checks if the sessionId is valid
    /**
     * Function to check if the sessionId is valid
     * Additionaly, it checks if the user is an admin
     * @param sessionId The user's sessionId
     * @return A pair, of which the first value indicates the session validity
     *         and the second value represents if the user is admin or not
     */
    suspend fun checkSessionValidityAsync(sessionId: String): Pair<Boolean, Boolean?> {
        return withContext(Dispatchers.IO) {
            try {
                val response = khttp.get(
                    url = activity.getString(R.string.PHP_server_IP) +
                            "/isAdmin",
                    params = mapOf("sessionId" to sessionId)
                )
                val responseText = response.text
                if (response.statusCode == 200) {
                    return@withContext Pair(true, responseText.toBoolean())
                }
                else if (responseText == "ERR_NO_SUCH_SESSION" || responseText == "ERR_NO_SUCH_USER") {
                    return@withContext Pair(false, null)
                } else {
                    return@withContext Pair(true, null)
                }
            } catch (e: Exception) {
                //return true, so the user won't get logged out if they are offline
                e.printStackTrace()
                return@withContext Pair(true, null)
            }
        }
    }

    fun loadLoginScreen() {
        val intent = Intent(activity, UsernameActivity::class.java)
        activity.startActivityForResult(intent, 101)
    }

    suspend fun getAndSendFirebaseToken() {
        val appUtils = AppUtilities(activity)
        val sessionId = AccountUtilities(activity).sessionId
        if (sessionId == null) {
            loadLoginScreen()
            return
        }
        //get the firebase registration token
        appUtils.getFirebaseToken { token ->
            if (token != null) {
                //send it to the server
                networkScope.launch {
                    val resp =
                        appUtils.sendFirebaseTokenToServerAsync(
                            token,
                            sessionId
                        )
                    if (!resp.await()) {
                        loadLoginScreen()
                    }
                }
            }
        }
    }

    fun hideAdminAreas(){
        val menu = activity.findViewById<NavigationView>(R.id.nav_view)
        val sendNotifItem = menu.menu.findItem(R.id.nav_send_notif)
        sendNotifItem.isVisible = false
        val adminChatItem = menu.menu.findItem(R.id.nav_adminchat)
        adminChatItem.isVisible = false
    }

    suspend fun loadHomeScreen(sessionId: String) {
        var feed = feedToHTML(sessionId)
        if (feed == null) {
            feed = "<i>Notificaties konden niet ingeladen worden</i>"
        }
        val appUtils = AppUtilities(activity)
        val programme = appUtils.readFromFile("programme.html")
        val info = appUtils.readFromFile("info.html")
        val html = """
            <big><big><big><u>Algemene info</u></big></big></big><br>
            $info<br><br>
            <big><big><big><u>Notificaties</u></big></big></big><br>
            $feed<br><br>
            <big><big><big><u>Programma</u></big></big></big><br>
            $programme
        """.trimIndent()
        loadMainWebview(html)
    }

}