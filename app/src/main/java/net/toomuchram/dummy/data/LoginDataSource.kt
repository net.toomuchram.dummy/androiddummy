package net.toomuchram.dummy.data


import android.content.Context
import android.content.res.Resources
import net.toomuchram.dummy.data.model.LoggedInUser
import java.io.IOException
import android.os.StrictMode
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import net.toomuchram.dummy.AccountUtilities
import net.toomuchram.dummy.R
import org.json.JSONObject


/**
 * Class that handles authentication w/ login credentials and retrieves user information.
 */
class LoginDataSource(val c: Context) {

    fun login(username: String, password: String): Result<LoggedInUser> {
        try {
            val policy = StrictMode.ThreadPolicy.Builder().permitAll().build()
            StrictMode.setThreadPolicy(policy)

            val result = postLoginEndpoint(username, password)
            when(result){
                "ERR_NO_SUCH_USER" -> {
                    return Result.Error(IOException("Gebruiker niet gevonden"))
                }
                "ERR_MISSING_USERNAME" -> {
                    return Result.Error(IOException("Geen gebruikersnaam ingevoerd"))
                }
                "ERR_USERNAME_TOO_LONG" -> {
                    return Result.Error(IOException("Te lange gebruikersnaam"))
                }
                "ERR_NO_PASSWORD" -> {
                    return Result.Error(IOException("Geen wachtwoord ingevoerd"))
                }
                "ERR_PASSWORD_TOO_LONG" -> {
                    return Result.Error(IOException("Te lang wachtwoord"))
                }
                "ERR_PASSWORD_TOO_SHORT" -> {
                    return Result.Error(IOException("Te kort wachtwoord"))
                }
            }
            //all other cases have been checked
            if(result.contains("ERR")){
                return Result.Error(IOException("Interne fout"))
            }

            //the endpoint returns a JSON payload containing name and sessionId
            //we only need to extract the name
            val obj = JSONObject(result)
            val sessionId = obj.getString("sessionId")

            val is_admin = obj.getInt("admin")

            var admin = false

            if(is_admin == 1){
                admin = true
            }

            val realUser = LoggedInUser(username, sessionId, admin)
            return Result.Success(realUser)

        } catch (e: Throwable) {
            return Result.Error(IOException("Error logging in", e))
        }
    }

    fun logout() = CoroutineScope(Dispatchers.IO).launch {
        val sessionId = AccountUtilities(c).sessionId
        // Post the logout endpoint
        khttp.post(
            url = c.getString(R.string.PHP_server_IP) + "/logout",
            data = mapOf("sessionId" to sessionId)
        )
        //Clear the local storage
        val sharedPref = c.getSharedPreferences(
            c.getString(R.string.prefs_file_key),
            Context.MODE_PRIVATE
        )
        with(sharedPref.edit()) {
            putString("username", null)
            putString("sessionId", null)
            putBoolean("admin", false)
            apply()
        }
    }
    private fun postLoginEndpoint(username: String, password: String): String {
        return khttp.post(
            url = c.getString(R.string.PHP_server_IP) + "/login",
            data = mapOf("username" to username, "password" to password)).text
    }

}

