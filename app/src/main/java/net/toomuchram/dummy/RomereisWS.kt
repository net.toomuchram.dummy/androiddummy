package net.toomuchram.dummy

import android.annotation.SuppressLint
import android.app.Service
import android.content.Context
import android.os.Handler
import android.util.Log
import com.google.android.gms.location.LocationCallback
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationResult
import com.google.android.gms.location.LocationServices
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import org.greenrobot.eventbus.EventBus
import org.java_websocket.client.WebSocketClient
import org.java_websocket.drafts.Draft
import org.java_websocket.handshake.ServerHandshake
import org.json.JSONException
import org.json.JSONObject
import java.net.URI

class RomereisWebsocket(
    serverUri: URI,
    draft: Draft,
    private val sessionId: String,
    private val applicationContext: Context,
    serviceObject: Service
) : WebSocketClient(serverUri, draft) {

    /*------------------*/
    /* Global variables */
    /*------------------*/


    private val fusedLocationClient = LocationServices.getFusedLocationProviderClient(serviceObject)
    private val appUtils = AppUtilities(applicationContext)
    private val notifUtils = NotificationUtilities(applicationContext)
    private val utils = RomereisWSUtilities(applicationContext)
    private val locationUtils = LocationUtilities(applicationContext)

    //Since it's being checked if the user is logged in at startup, it's very unlikely that the user is not logged in.
    //Therefore, we can set the default value of loggedIn to true.
    var loggedIn: Boolean = true
    var connected: Boolean = false



    /*---------------*/
    /* The websocket */
    /*---------------*/

    override fun onOpen(handshakedata: ServerHandshake) {
        println("opened connection")
        send("""
            {
            "sessionId": "$sessionId",
            "channel":"authentication"
            }
            """.trimIndent()
        )
        connected = true
    }

    override fun onClose(code: Int, reason: String, remote: Boolean) {
        println("disconnected from server")
        println("trying to reconnect in ten seconds")
        if(loggedIn){
            CoroutineScope(Dispatchers.Main).launch {
                delay(10000)
                println("reconnecting...")
                reconnect()
            }
        }
        connected = false
        stopLocationUpdates()
    }

    override fun onMessage(message: String) {
        try{
            val obj = JSONObject(message)
            val channel = obj.getString("channel")
            if(channel == "misc"){
                val message1 = obj.getString("message")
                if(message1 == "MULTIPLE_SESSIONS") {
                    notifUtils.notifyMultipleClients()
                }
            }
            else if(channel == "authentication") {
                val message1 = obj.getString("message")
                if(message1 == "AUTH_SUCCESS"){
                    //Authentication was successful, start sending locations
                    loggedIn = true
                    startLocationUpdates()
                }

            }
            else if(channel == "errors"){
                val error = obj.getString("error")
                if(error == "ERR_NO_SUCH_SESSION" || error == "ERR_NO_SUCH_USER"){
                    loggedIn = false
                    //notify the user
                    notifUtils.notifyNotLoggedIn()
                }
            }
            else if(channel == "locations" || channel == "msg" || channel == "adminmsg"){
                EventBus.getDefault().post(MessageEvent(message))
            }
        }
        catch (e: JSONException){
            e.printStackTrace()
        }
    }

    override fun onError(ex: Exception) {
        println("an error occurred:$ex")
        ex.printStackTrace()
    }


    //LOCATIONS
    private val locationRequest = LocationRequest()
        .setInterval(10000)
        .setFastestInterval(5000)
        .setSmallestDisplacement((5.0).toFloat())
        .setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY)

    @SuppressLint("MissingPermission")
    fun startLocationUpdates() {
        val mainHandler = Handler(applicationContext.mainLooper)
        mainHandler.post{
            if(appUtils.verifyLocationPermissions()){

                fusedLocationClient.requestLocationUpdates(locationRequest,
                    locationCallback,
                    null /* Looper */)
            }
        }
    }

    fun oneTimeLocation(){
        appUtils.getOneTimeLocation { location ->
            if(location != null && location != locationUtils.getLastLocation()){
                locationUtils.setLastLocation(location)
                utils.sendLocation(location, sessionId, this)
            }
        }
    }

    private fun stopLocationUpdates() {
        fusedLocationClient.removeLocationUpdates(locationCallback)
    }


    private val locationCallback = object : LocationCallback() {
        override fun onLocationResult(locationResult: LocationResult?) {
            locationResult ?: return
            for (location in locationResult.locations){
                if(location != locationUtils.getLastLocation()){
                    locationUtils.setLastLocation(location)
                    utils.sendLocation(
                        location,
                        sessionId,
                        this@RomereisWebsocket
                    )
                }
            }
        }
    }

}