package net.toomuchram.dummy

import android.content.Context

class AccountUtilities(c: Context) {

    val sessionId: String?
    val username: String?
    val admin: Boolean

    init {
        val sharedPref = c.getSharedPreferences(c.getString(R.string.prefs_file_key), Context.MODE_PRIVATE)
        sessionId = sharedPref.getString("sessionId", "")
        username = sharedPref.getString("username", "")
        admin = sharedPref.getBoolean("admin", false)
    }
}