package net.toomuchram.dummy

import android.Manifest
import android.annotation.SuppressLint
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import org.json.JSONArray
import java.io.*
import android.view.*
import android.widget.*
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.view.isVisible
import com.google.gson.Gson
import kotlinx.coroutines.*
import net.toomuchram.dummy.ChatActivity.MessagePosition.*
import net.toomuchram.dummy.ui.login.UsernameActivity
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import org.json.JSONObject


class ChatActivityUtilities(private val activity: ChatActivity, private val adminchat: Boolean) {

    private val networkScope = CoroutineScope(Dispatchers.IO)
    private val activityScope = CoroutineScope(Dispatchers.Main)
    private var accountUtils = AccountUtilities(activity)
    val loadedMessages = hashMapOf<Int, ChatActivity.ChatMessage>()

    fun <T : View> findViewById(id: Int): View {
        return activity.findViewById<T>(id)
    }

    @SuppressLint("ClickableViewAccessibility")
    fun initChat() = activity.run {
        val activityIndicator =
            findViewById<ProgressBar>(R.id.messageProgressBar)
        val sendButton = findViewById<Button>(R.id.button_chatbox_send)
        val gestureDetector = GestureDetector(
            activity,
            ChatSendButtonClickListener(activity, sendButton)
        )
        sendButton.setOnTouchListener { _, event ->
            gestureDetector.onTouchEvent(event)
        }

        val sessionId = accountUtils.sessionId
        if (sessionId != null) {
            activityScope.launch {
                var msgs: List<ChatActivity.ChatMessage>? = null
                while (msgs == null || msgs.count() == 0) {
                    msgs = getMessageHistoryAsync(
                        sessionId,
                        loadedDays + 2,
                        loadedDays
                    )
                    loadedDays += 2
                }
                loadMessageHistory(msgs)
                activityIndicator.visibility = View.GONE

                if (msgs[0].id != 1) {
                    val chatContainer =
                        findViewById<LinearLayout>(R.id.chat_content)
                    chatContainer.addView(createLoadMoreMessagesButton(), 0)
                }
            }
            activity.run {
                EventBus.getDefault().register(this)
            }
        }
    }

    class ChatSendButtonClickListener(
        private val activity: ChatActivity,
        private val sendButton: View
    ) : GestureDetector.SimpleOnGestureListener() {
        override fun onSingleTapUp(e: MotionEvent?): Boolean {
            activity.onClickSend(sendButton)
            return super.onSingleTapUp(e)
        }

        override fun onLongPress(e: MotionEvent?) {
            if (activity.utils.verifyCameraPerms()) {
                activity.takePhoto()
            } else {
                ActivityCompat.requestPermissions(
                    activity,
                    arrayOf(Manifest.permission.CAMERA),
                    101
                )

            }
        }
    }


    private fun createLoadMoreMessagesButton(): Button {
        val button = Button(activity)
        button.text = activity.getString(R.string.load_more_messages)
        button.setOnClickListener {
            loadMoreMessages(it)
        }
        return button
    }


    private fun loadMoreMessages(loadMoreMessagesBtn: View? = null) {
        activity.run {
            //Get the messages from the specified day from the server
            val sessionId = accountUtils.sessionId ?: return@run

            loadMoreMessagesBtn?.isEnabled = false
            CoroutineScope(Dispatchers.Main).launch {
                //Fetch msgs and check if there are any
                val msgs = getMessageHistoryAsync(
                    sessionId,
                    loadedDays + 2,
                    loadedDays
                )
                    ?: return@launch

                // "Delete" the original button
                loadMoreMessagesBtn?.isVisible = false

                for (i in msgs.count() - 1 downTo 0) {
                    val chatmessage = msgs[i]
                    val previousMessage = if (i == 0) {
                        null
                    } else {
                        msgs[i - 1]
                    }

                    if (
                        previousMessage != null &&
                        previousMessage.user == chatmessage.user
                    ) {

                        addMessage(message = chatmessage, withHeader = false, position = Top)
                    } else {

                        addMessage(message = chatmessage, position = Top)
                    }


                }
                loadedDays += 2
                activityScope.launch {
                    addNewLoadMoreMessagesButton(msgs)
                }
            }
        }
    }

    /**
     * Function to add a new load more messages button.
     * Apparently, it also loads more messages if it detects that no messages have come in.
     * @param msgs The messages to load
     */
    private fun addNewLoadMoreMessagesButton(msgs: List<ChatActivity.ChatMessage>) {
        try {
            if(msgs.count() > 0) {
                //The very first message has the ID of 1
                if (msgs[0].id > 1) {
                    activity.run {
                        //Create a new 'Load more messages' button if needed
                        val chatContainer = findViewById<LinearLayout>(R.id.chat_content)
                        chatContainer.addView(
                            createLoadMoreMessagesButton(), 0
                        )
                    }
                }
            }
            else {
                loadMoreMessages()
            }

        } catch (e: Exception) {
            //The most likely case is that there were no messages
            //Thus, the button can be reenabled
            e.printStackTrace()

            //Keep on trying
            //It will stop when the last message is reached
            loadMoreMessages()
        }
    }


    private suspend fun getMessageHistoryAsync(sessionId: String, days: Int = 1, to: Int = 0): List<ChatActivity.ChatMessage>? {
        return withContext(Dispatchers.IO) {
            return@withContext try {
                val url = if (adminchat) {
                    activity.getString(R.string.PHP_server_IP) +
                            "/getMessages?sessionId=$sessionId&days=$days&to=$to&type=admin"
                } else {
                    activity.getString(R.string.PHP_server_IP) +
                            "/getMessages?sessionId=$sessionId&days=$days&to=$to"
                }
                val resp = khttp.get(
                    url = url
                ).text

                if (resp == "ERR_MISSING_SESSIONID" || resp == "ERR_NO_SUCH_SESSION") {
                    activityScope.launch {
                        val intent =
                            Intent(activity, UsernameActivity::class.java)
                        activity.startActivity(intent)
                    }
                    null
                } else if (resp == "ERR_UNAUTHORISED") {
                    activityScope.launch {
                        Toast.makeText(
                            activity,
                            activity.getString(R.string.unauthorised),
                            Toast.LENGTH_LONG
                        ).show()
                    }
                    null
                } else {
                    val gson = Gson()
                    val chatMessages = mutableListOf<ChatActivity.ChatMessage>()
                    val parsedJsonArray = JSONArray(resp)
                    for (i in 0 until parsedJsonArray.length()) {
                        val obj = parsedJsonArray.getJSONObject(i)
                        chatMessages.add(gson.fromJson(obj.toString(), ChatActivity.ChatMessage::class.java))
                    }
                    chatMessages
                }

            } catch (e: Exception) {
                e.printStackTrace()
                null
            }
        }
    }

    private fun loadMessageHistory(messageHistory: List<ChatActivity.ChatMessage>) {

        for (i in 0 until messageHistory.count()) {
            val message = messageHistory[i]
            loadedMessages[i] = message

            if(i >= 1) {
                val previousMessage = messageHistory[i - 1]
                if (previousMessage.user == message.user) {
                    activity.addMessage(message = message, withHeader = false)
                } else {
                    activity.addMessage(message)
                }
            } else {
                activity.addMessage(message)
            }
        }

    }


    fun sendMessage(message: String, sessionId: String) =
        networkScope.launch {
            try {
                val params = if (adminchat) {
                    mapOf(
                        "sessionId" to sessionId,
                        "message" to message,
                        "type" to "admin"
                    )
                } else {
                    mapOf(
                        "sessionId" to sessionId,
                        "message" to message
                    )
                }
                val resp = khttp.post(
                    url = activity.getString(R.string.PHP_server_IP) +
                            "/sendMessage",
                    data = params
                ).text
                if (resp == "ERR_TOO_MANY_CHARACTERS") {
                    activityScope.launch {
                        Toast.makeText(
                            activity,
                            activity.getString(R.string.too_many_characters),
                            Toast.LENGTH_LONG
                        ).show()
                    }
                } else if (
                    resp == "ERR_MISSING_SESSIONID" ||
                    resp == "ERR_NO_SUCH_SESSION" ||
                    resp == "ERR_NO_SUCH_USER"
                ) {
                    activityScope.launch {
                        val intent = Intent(activity, UsernameActivity::class.java)
                        activity.startActivity(intent)
                    }

                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }


    fun verifyCameraPerms(): Boolean {
        return (ContextCompat.checkSelfPermission(
            activity,
            Manifest.permission.CAMERA
        ) == PackageManager.PERMISSION_GRANTED)
    }



}