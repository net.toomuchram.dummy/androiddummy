package net.toomuchram.dummy

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.graphics.BitmapFactory
import android.view.View
import android.widget.ImageView
import android.graphics.Bitmap
import android.graphics.Matrix
import android.widget.Toast
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import net.toomuchram.dummy.ui.login.UsernameActivity
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.FileOutputStream
import java.io.IOException


class PhotoEditActivity : AppCompatActivity() {

    private lateinit var editorView: ImageView
    private lateinit var bitmap: Bitmap
    private lateinit var accountUtils: AccountUtilities
    private lateinit var caption: String
    private var adminchat: Boolean = false
    private val activityScope = CoroutineScope(Dispatchers.Main)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_photo_edit)

        val byteArray = intent.getByteArrayExtra("bitmap")
        bitmap = BitmapFactory.decodeByteArray(byteArray, 0, byteArray!!.size)
        caption = intent.getStringExtra("caption") ?: ""

        adminchat = intent.getBooleanExtra("adminchat", false)

        accountUtils = AccountUtilities(applicationContext)

        editorView = findViewById(R.id.editorView)

        editorView.setImageBitmap(bitmap)

    }

    fun onClickRotateImage(v: View) {
        val matrix = Matrix()

        matrix.postRotate(90F)

        val scaledBitmap =
            Bitmap.createScaledBitmap(bitmap, bitmap.width, bitmap.height, true)

        val rotatedBitmap = Bitmap.createBitmap(
            scaledBitmap,
            0,
            0,
            scaledBitmap.width,
            scaledBitmap.height,
            matrix,
            true
        )
        bitmap = rotatedBitmap
        editorView.setImageBitmap(rotatedBitmap)
    }

    fun sendEditedImage(v: View) {
        val sessionId = accountUtils.sessionId

        if (sessionId != null) {
            val activityScope = CoroutineScope(Dispatchers.Main)
            activityScope.launch {
                uploadFile(
                    caption,
                    bitmap,
                    sessionId
                )
                Toast.makeText(
                    applicationContext,
                    getString(R.string.warning_photos_take_long),
                    Toast.LENGTH_SHORT
                ).show()
                finish()
            }
        } else {
            val intent = Intent(this, UsernameActivity::class.java)
            startActivity(intent)
        }

    }

    suspend fun uploadFile(message: String, image: Bitmap, sessionId: String) {
        withContext(Dispatchers.IO) {
            val multipart = MultipartUtility(
                getString(R.string.PHP_server_IP) + "/sendMessage",
                "UTF-8"
            )
            multipart.addFormField("sessionId", sessionId)
            multipart.addFormField("message", message)
            if (adminchat) {
                multipart.addFormField("type", "admin")
            }
            //create a file to write bitmap data
            val f = File(cacheDir, "temp.png")
            f.createNewFile()

            //Convert bitmap to byte array
            val bos = ByteArrayOutputStream()
            image.compress(
                Bitmap.CompressFormat.PNG,
                0 /*ignored for PNG*/,
                bos
            )
            val bitmapdata = bos.toByteArray()

            //write the bytes in file
            val fos = FileOutputStream(f)
            fos.write(bitmapdata)
            fos.flush()
            fos.close()
            multipart.addFilePart("img", f)
            try {
                multipart.finish()
            } catch (e: IOException) {
                e.printStackTrace()
                activityScope.launch {
                    Toast.makeText(
                        applicationContext,
                        getString(R.string.uploading_failed),
                        Toast.LENGTH_LONG
                    ).show()
                }
            }
        }

    }
}
