# net.toomuchram.dummy
#### Android version

[Google Play Store](https://play.google.com/store/apps/details?id=net.toomuchram.dummy)


Dit is de officiële Git-repository voor de Huygens Lyceum Romereis 2020 android app. De app is beschikbaar op de Play Store of via Yalp Store, of je kan hem compileren vanaf de broncode.

## Doneren

Ik heb toetsweekresultaten en mijn halve zomervakantie hiervoor opgeofferd, dus een donatie wordt op prijs gesteld. Doneren kan via [PayPal](https://paypal.me/toomuchram).
